package com.hjy.http.upload;

import android.text.TextUtils;

import com.hjy.http.upload.listener.OnUploadListener;
import com.hjy.http.upload.progressaware.ProgressAware;

import java.util.HashMap;

/**
 * Created by Administrator on 2018/4/25.
 */

public class UploadUtil {
    private static final String TAG = "UploadUtil";

    private static final String UPLOAD_URL = "http://myzexabox.6655.la:8020/zbx" +
            "/file/upload";
    public static void upload(ProgressAware progressAware, /*Map<String, String> paramMap,*//* String id,*/
                              String filePath, String mimeType, OnUploadListener apiCallback,
                              /*OnUploadProgressListener uploadProgressListener,*/ UploadOptions options){
//        if (images.size()<= 0){
//            return;
//        }
//        Toast.makeText(this,"已添加到上传列表",Toast.LENGTH_SHORT).show();
//        for (LocalMedia media:images){
            if (!TextUtils.isEmpty(filePath)){
                String zfsFile = "";
                int start=filePath.lastIndexOf("/");
                if (start != -1){
                    zfsFile = filePath.substring(start+1);
                }
                java.util.Map paramMap = new HashMap<>();
                paramMap.put("path","/");
                paramMap.put("zfsFile","@"+zfsFile);


                FileUploadManager.getInstance().uploadFile(progressAware,paramMap,
                        filePath, filePath,String.valueOf(mimeType),
                        UPLOAD_URL, apiCallback,options);
            }
//        }
    }
    public static void upload( String filePath, String mimeType, OnUploadListener apiCallback,
                              UploadOptions options){
           upload(null,filePath,mimeType,apiCallback,options);
    }
    public static void upload( String filePath, String mimeType, OnUploadListener apiCallback){
           upload(null,filePath,mimeType,apiCallback,null);
    }
}
