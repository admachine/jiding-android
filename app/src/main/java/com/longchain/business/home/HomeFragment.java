package com.longchain.business.home;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.hh.base.util.JsonUtils;
import com.longchain.R;
import com.longchain.business.mylongchain.MyChainActivity;
import com.longchain.business.search.SearchActivity;
import com.longchain.business.tag.MoreTagsActivity;
import com.longchain.common.base.BaseFragment;
import com.longchain.manager.FileManager;
import com.tsy.sdk.myokhttp.response.JsonResponseHandler;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by 28367 on 2018/3/24.
 */

public class HomeFragment extends BaseFragment {

    private static final String TAG = "HomeFragment";
    @BindView(R.id.pic_num)
    TextView mPicNum;

    @BindView(R.id.video_num)
    TextView mVideoNum;

    @BindView(R.id.file_num)
    TextView mFileNum;

    @BindView(R.id.music_num)
    TextView mMusicNum;

    @BindView(R.id.all_num)
    TextView mAllNum;

    @Override
    protected int setLayoutResourceID() {
        return R.layout.home_fragment;
    }

    @Override
    protected void setUpView() {

    }

    @Override
    public void onResume() {
        super.onResume();
        updateCount();
    }

    @Override
    protected void setUpData() {

    }

    private void updateCount() {
        FileManager.getInstance().getFileCount(new JsonResponseHandler() {

            @Override
            public void onSuccess(int statusCode, JSONObject response) {
                super.onSuccess(statusCode, response);
                Log.d(TAG,"get image count success :"+response.toString());

                JSONObject data = JsonUtils.getJSONObject(response,"data");
                JSONObject count = JsonUtils.getJSONObject(data,"count");
                mPicNum.setText(JsonUtils.getString(count,"image"));
                mVideoNum.setText(JsonUtils.getString(count,"video"));
                mMusicNum.setText(JsonUtils.getString(count,"audio"));
                mAllNum.setText(JsonUtils.getString(count,"total"));
                mFileNum.setText(JsonUtils.getString(count,"text"));

            }

            @Override
            public void onFailure(int statusCode, String error_msg) {

            }
        });
    }

    @OnClick({ R.id.search_btn, R.id.gift_btn
            ,R.id.go_more_tags
            ,R.id.file_framelayout
            ,R.id.video_framelayout
            ,R.id.music_framelayout
            ,R.id.all_file
            ,R.id.pic_framelayut})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.search_btn:
                startActivity(new Intent(getActivity(), SearchActivity.class));
                break;
            case R.id.go_more_tags:
                startActivity(new Intent(getActivity(), MoreTagsActivity.class));
                break;
            case R.id.gift_btn:
                startActivity(new Intent(getActivity(), MyChainActivity.class));
                break;
            case R.id.pic_framelayut:
                Intent picIntent = new Intent(getActivity(), SearchActivity.class);
                picIntent.putExtra(SearchActivity.FROM,FileManager.STATUS_IMAGE);
                startActivity(picIntent);
                break;
            case R.id.file_framelayout:
                Intent fileIntent = new Intent(getActivity(), SearchActivity.class);
                fileIntent.putExtra(SearchActivity.FROM,FileManager.STATUS_TEXT);
                startActivity(fileIntent);
                break;
            case R.id.music_framelayout:
                Intent musicIntent = new Intent(getActivity(), SearchActivity.class);
                musicIntent.putExtra(SearchActivity.FROM,FileManager.STATUS_AUDIO);
                startActivity(musicIntent);
                break;
            case R.id.video_framelayout:
                Intent videoIntent = new Intent(getActivity(), SearchActivity.class);
                videoIntent.putExtra(SearchActivity.FROM,FileManager.STATUS_VIDEO);
                startActivity(videoIntent);
                break;
            case R.id.all_file:
                Intent allIntent = new Intent(getActivity(), SearchActivity.class);
                allIntent.putExtra(SearchActivity.FROM,FileManager.STATUS_ALL);
                startActivity(allIntent);
                break;

        }
    }
}
