package com.longchain.business.cloudplayer;

import com.longchain.R;
import com.longchain.common.base.BaseActivity;

/**
 * Created by 28367 on 2018/4/1.
 */

public class CloudPlayerGuideActivity extends BaseActivity {
    @Override
    protected int initLayoutId() {
        return R.layout.activity_cloud_player_guide;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {

    }
}
