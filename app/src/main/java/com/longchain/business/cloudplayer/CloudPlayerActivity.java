package com.longchain.business.cloudplayer;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.longchain.R;
import com.longchain.common.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by 28367 on 2018/4/1.
 */

public class CloudPlayerActivity extends BaseActivity {
    @BindView(R.id.dlna_close_btn)
    TextView mDlnaCloseBtn;
    @BindView(R.id.samba_close_btn)
    TextView mSambaCloseBtn;
    @BindView(R.id.hdmi_close_btn)
    TextView mHdmiCloseBtn;
    @BindView(R.id.cloud_player_guide)
    TextView mCloudPlayerGuide;

    @Override
    protected int initLayoutId() {
        return R.layout.activity_cloud_player;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {

    }

    @OnClick({R.id.dlana_layout, R.id.samba_layout, R.id.hdmi_layout, R.id.cloud_player_guide})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.dlana_layout:
                break;
            case R.id.samba_layout:
                break;
            case R.id.hdmi_layout:
                break;
            case R.id.cloud_player_guide:
                startActivity(new Intent(this,CloudPlayerGuideActivity.class));
                break;
        }
    }
}
