package com.longchain.business.equipmentmgr;

import com.longchain.R;
import com.longchain.common.base.BaseActivity;

/**
 * Created by 28367 on 2018/4/1.
 */

public class EquipmentActivity extends BaseActivity{
    @Override
    protected int initLayoutId() {
        return R.layout.activity_equipment;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {

    }
}
