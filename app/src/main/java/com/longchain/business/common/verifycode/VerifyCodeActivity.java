package com.longchain.business.common.verifycode;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.widget.TextView;

import com.longchain.R;
import com.longchain.common.base.BaseActivity;
import com.longchain.common.widget.VerifyCodeEt;
import com.longchain.business.common.setpwd.SetPwdActivity;
import com.longchain.business.common.iview.IGetVerifyCode;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by 28367 on 2018/3/18.
 */

public class VerifyCodeActivity extends BaseActivity implements IGetVerifyCode {
    @BindView(R.id.send_to_phone)
    TextView mSendToPhone;
    @BindView(R.id.pwd_et)
    VerifyCodeEt mPwdEt;
    @BindView(R.id.resend_verify_code)
    TextView mResendVerifyCode;
    private String mPhoneNum;
    private MyCountDownTimer mMyCountDownTimer;


    @Override
    protected int initLayoutId() {
        return R.layout.activity_verify_code;
    }

    @Override
    protected void initView() {
        mSendToPhone.setText(String.format(getString(R.string.verify_code_to_num), mPhoneNum));
    }

    @Override
    protected void initData() {

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            mPhoneNum = bundle.getString("phoneNum");
        }
        mPwdEt.setOnCompleteListener(new VerifyCodeEt.Listener() {
            @Override
            public void onComplete(String content) {
                //TODO loading
                verifyCode(content);
            }
        });
        mMyCountDownTimer = new MyCountDownTimer(60000, 1000);
        mMyCountDownTimer.start();
    }

    private void verifyCode(String content) {
        Bundle bundle = new Bundle();
        bundle.putString("phoneNum", mPhoneNum);
        bundle.putString("verify_code", content);
        Intent intent = new Intent(this, SetPwdActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @OnClick(R.id.resend_verify_code)
    public void onSendVerifyCode() {
        mMyCountDownTimer.cancel();
        mMyCountDownTimer.start();
    }

    @Override
    public void getVerifyCode(String num) {

    }


    private class MyCountDownTimer extends CountDownTimer {

        public MyCountDownTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long l) {
            mResendVerifyCode.setClickable(false);
            mResendVerifyCode.setText(String.format(getString(R.string.resend_verify_codes), (int) (l / 1000)));

        }

        @Override
        public void onFinish() {
            mResendVerifyCode.setText("重新发送验证码");
            mResendVerifyCode.setClickable(true);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mMyCountDownTimer != null) {
            mMyCountDownTimer.cancel();
            mMyCountDownTimer = null;
        }
    }
}
