package com.longchain.business.common.welcome;



import com.longchain.R;
import com.longchain.common.base.BaseActivity;

import butterknife.BindView;

/**
 * Created by 28367 on 2018/3/17.
 */

public class GuideActivity extends BaseActivity {

    @BindView(R.id.content_FrameLayout)
    PageFrameLayout mGuideLayout;


    @Override
    protected int initLayoutId() {
        return R.layout.activity_guide;
    }

    @Override
    protected void initView() {
        mGuideLayout = (PageFrameLayout) findViewById(R.id.content_FrameLayout);
        // 设置资源文件和选中圆点
        mGuideLayout.setUpViews(new int[]{
                R.layout.page_tab1,
                R.layout.page_tab2,
                R.layout.page_tab3,
                R.layout.page_tab4
        }, R.drawable.banner_on, R.drawable.banner_off);
    }

    @Override
    protected void initData() {

    }
}
