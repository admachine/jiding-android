package com.longchain.business.common.login;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hh.base.util.UIUtil;
import com.longchain.business.HomeActivity;
import com.longchain.R;
import com.longchain.common.base.BaseActivity;
import com.longchain.business.common.verifycode.GetVerifyCodeActivity;
import com.longchain.manager.AccountManager;
import com.tsy.sdk.myokhttp.response.JsonResponseHandler;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by 28367 on 2018/3/17.
 */

public class LoginActivity extends BaseActivity {
    @BindView(R.id.tv_input_id)
    EditText mAccountEt;
    @BindView(R.id.clear_btn)
    ImageView mClearBtn;
    @BindView(R.id.password_input)
    EditText mPasswordEt;
    @BindView(R.id.eye_btn)
    CheckBox mEyeBtn;
    @BindView(R.id.login_btn)
    Button mLoginBtn;
    @BindView(R.id.account_layout)
    LinearLayout mAccountLayout;
    @BindView(R.id.password_layout)
    LinearLayout mPasswordLayout;
    @BindView(R.id.forget_pwd)
    TextView mForgetPwd;
    @BindView(R.id.register_account)
    TextView mRegisterAccount;

    private boolean mAccountEnabled = false;
    private boolean mPwdEnabled = false;

    @Override
    protected int initLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    protected void initView() {

        mAccountEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int before, int count) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                checkAccountEnable(charSequence);
                if (charSequence != null && charSequence.length() > 0) {
                    mClearBtn.setVisibility(View.VISIBLE);
                } else {
                    mClearBtn.setVisibility(View.GONE);
                }
                setLoginBtnStyle();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mPasswordEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int before, int count) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                checkPwdEnable(charSequence);
                setLoginBtnStyle();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        checkAccountEnable(mAccountEt.getText());
        checkPwdEnable(mPasswordEt.getText());
        setLoginBtnStyle();
    }

    private void checkAccountEnable(CharSequence charSequence) {
        if (charSequence != null && charSequence.length() >= 11) {
            mAccountEnabled = true;
        } else {
            mAccountEnabled = false;
        }
    }

    private void checkPwdEnable(CharSequence charSequence) {
        if (charSequence != null && charSequence.length() >= 6) {
            mPwdEnabled = true;
        } else {
            mPwdEnabled = false;
        }
    }


    private void setLoginBtnStyle() {
        if (mPwdEnabled && mAccountEnabled) {
            mLoginBtn.setEnabled(true);
            mLoginBtn.setTextColor(getResources().getColor(R.color.text_color));
        } else {
            mLoginBtn.setEnabled(false);
            mLoginBtn.setTextColor(getResources().getColor(R.color.white));
        }
    }

    @Override
    protected void initData() {

    }

    @OnClick(R.id.register_account)
    public void onRegisterAccount() {
        Intent intent = new Intent(this, GetVerifyCodeActivity.class);
        Bundle bundle = new Bundle();
        bundle.putBoolean("register",true);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @OnClick(R.id.clear_btn)
    public void onClearAccount() {
        mAccountEt.setText("");
    }

    @OnClick(R.id.login_btn)
    public void onLogin() {

        UIUtil.loadingShow(this);
        AccountManager.getInstance().login(mAccountEt.getText().toString(), mPasswordEt.getText().toString(), new JsonResponseHandler() {
            @Override
            public void onSuccess(int statusCode, JSONObject response) {
                UIUtil.loadingCancel();
                startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                finish();
            }

            @Override
            public void onFailure(int i, String s) {
                UIUtil.loadingCancel();
                Toast.makeText(mContext,String.format("登录失败，原因：%s，code = %d",s,i),Toast.LENGTH_LONG).show();
            }
        });

    }

    @OnClick(R.id.forget_pwd)
    public void onResetPwd() {
        Intent intent = new Intent(this, GetVerifyCodeActivity.class);
        Bundle bundle = new Bundle();
        bundle.putBoolean("register",false);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @OnClick(R.id.eye_btn)
    public void onShowPwd() {
        if (mEyeBtn.isChecked()) {
            mPasswordEt.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        } else {
            mPasswordEt.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
        mPasswordEt.setSelection(mPasswordEt.getText().length());
    }
}
