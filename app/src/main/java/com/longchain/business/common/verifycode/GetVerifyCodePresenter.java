package com.longchain.business.common.verifycode;

import android.util.Log;

import com.longchain.common.base.BasePresenter;
import com.longchain.business.common.iview.IGetVerifyCode;
import com.tsy.sdk.myokhttp.response.JsonResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by 28367 on 2018/3/19.
 */

public class GetVerifyCodePresenter extends BasePresenter {

    private IGetVerifyCode mIGetVerifyCode;
    private final String TAG = getClass().getSimpleName();
    private static final String GET_VERIFY_CODE = "/api/v0/sms";
    public GetVerifyCodePresenter(IGetVerifyCode iGetVerifyCode) {
        super();
        mIGetVerifyCode = iGetVerifyCode;
    }

    public void getVerifyCode(String num){
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("id", "123");
            JSONObject user = new JSONObject();
            user.put("phone", num);
            jsonObject.put("user", user);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mMyOkHttp.post()
                .url(GET_VERIFY_CODE)
                .jsonParams(jsonObject.toString())
                .tag(this)
                .enqueue(new JsonResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, JSONObject response) {
                        Log.d(TAG, "doPostJSON onSuccess JSONObject:" + response);
                    }

                    @Override
                    public void onSuccess(int statusCode, JSONArray response) {
                        Log.d(TAG, "doPostJSON onSuccess JSONArray:" + response);
                    }

                    @Override
                    public void onFailure(int statusCode, String error_msg) {
                        Log.d(TAG, "doPostJSON onFailure:" + error_msg);
                    }
                });
    }
}
