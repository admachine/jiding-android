package com.longchain.business.common.welcome;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;


import com.longchain.R;
import com.longchain.common.utils.DensityUtil;

import java.util.ArrayList;
import java.util.List;


public class PageFrameLayout extends FrameLayout implements ViewPager.OnPageChangeListener {
    private List<PageFragment> fragments = new ArrayList<>();
    private ImageView[] mGuideIvs = null;
    private LinearLayout mDotLl;
    private int mDotOn, mDotOff;

    public PageFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    /**
     * 设置资源文件并初始化控件
     *
     * @param layoutIds
     */
    public void setUpViews(int layoutIds[], int dotOn, int dotOff) {
        this.mDotOn = dotOn;
        this.mDotOff = dotOff;
        mGuideIvs = new ImageView[layoutIds.length];
        mDotLl = new LinearLayout(getContext());
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                DensityUtil.dip2px(getContext(), 35));
        mDotLl.setGravity(Gravity.CENTER);
        params.gravity = Gravity.BOTTOM;
        mDotLl.setLayoutParams(params);
        mDotLl.setOrientation(LinearLayout.HORIZONTAL);

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(8, 0, 8, 0);

        for (int i = 0; i < layoutIds.length; i++) {
            PageFragment pageFragment = new PageFragment();
            Bundle args = new Bundle();
            args.putInt("index", i);
            args.putInt("count", layoutIds.length);
            args.putInt("layoutId", layoutIds[i]);

            pageFragment.setArguments(args);
            fragments.add(pageFragment);
            ImageView view = new ImageView(getContext());
            view.setImageResource(dotOn);
            view.setLayoutParams(lp);
            mGuideIvs[i] = view;
            mDotLl.addView(mGuideIvs[i]);
        }

        setSelectVp(0);
        GuideActivity activity = (GuideActivity) getContext();
        ViewPager viewPager = new ViewPager(getContext());
        viewPager.setId(R.id.id_page);
        viewPager.setAdapter(new PageFragmentAdapter(activity.getSupportFragmentManager(), fragments));
        viewPager.addOnPageChangeListener(this);
        addView(viewPager);
        addView(mDotLl);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (position == fragments.size() - 1) {
            mDotLl.setAlpha(0);
        } else {
            mDotLl.setAlpha(1);
        }

    }

    @Override
    public void onPageSelected(int position) {
        setSelectVp(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    /**
     * 切换轮播图点
     *
     * @param index
     */
    public void setSelectVp(int index) {
        for (int i = 0; i < mGuideIvs.length; i++) {
            if (i == index) {
                mGuideIvs[i].setImageResource(mDotOn);
            } else {
                mGuideIvs[i].setImageResource(mDotOff);
            }
        }
    }
}
