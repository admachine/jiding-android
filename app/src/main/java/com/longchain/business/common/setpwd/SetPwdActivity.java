package com.longchain.business.common.setpwd;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.longchain.R;
import com.longchain.business.HomeActivity;
import com.longchain.common.base.BaseActivity;
import com.longchain.manager.AccountManager;
import com.tsy.sdk.myokhttp.response.JsonResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by 28367 on 2018/3/18.
 */

public class SetPwdActivity extends BaseActivity {
    @BindView(R.id.tv_new_pwd)
    EditText mNewPwd;
    @BindView(R.id.clear_btn)
    ImageView mClearBtn;
    @BindView(R.id.next_btn)
    Button mNextBtn;
    @BindView(R.id.eye_btn)
    CheckBox mEyeBtn;

    private String mPhoneNum;
    private String verifyCode;

    @Override
    protected int initLayoutId() {
        return R.layout.activity_set_password;
    }

    @Override
    protected void initView() {
        mNewPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int before, int count) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                if (charSequence != null) {
                    mClearBtn.setVisibility(View.VISIBLE);
                    if (charSequence.length() >= 6) {
                        mNextBtn.setEnabled(true);
                        mNextBtn.setTextColor(getResources().getColor(R.color.text_color));
                    } else {
                        mNextBtn.setTextColor(getResources().getColor(R.color.white));
                        mNextBtn.setEnabled(false);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    protected void initData() {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            mPhoneNum = bundle.getString("phoneNum");
            verifyCode = bundle.getString("verify_code");
        }
    }

    @OnClick(R.id.eye_btn)
    public void onEyesBtnClicked() {
        if(mEyeBtn.isChecked()){
            mNewPwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }else {
            mNewPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
        mNewPwd.setSelection(mNewPwd.getText().length());
    }

    @OnClick(R.id.next_btn)
    public void onNextBtnClicked() {
        //TODO 设置密码
        AccountManager.getInstance().register(mPhoneNum, mNewPwd.getText().toString(), new JsonResponseHandler() {
            @Override
            public void onSuccess(int statusCode, JSONObject response) {
                startActivity(new Intent(SetPwdActivity.this, HomeActivity.class));
                finish();
            }


            @Override
            public void onFailure(int i, String s) {
                Toast.makeText(mContext,String.format("注册失败，原因：%s，code = %d",s,i),Toast.LENGTH_LONG).show();
            }
        });
    }

    @OnClick(R.id.clear_btn)
    public void onClearBtnViewClicked() {
        mNewPwd.setText("");
    }
}
