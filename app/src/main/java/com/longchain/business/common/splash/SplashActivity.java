package com.longchain.business.common.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.longchain.R;
import com.longchain.common.base.BaseActivity;
import com.longchain.common.utils.Config;
import com.longchain.common.utils.Constants;
import com.longchain.business.common.login.LoginActivity;
import com.longchain.business.common.welcome.GuideActivity;

/**
 * Created by 28367 on 2018/3/17.
 */

public class SplashActivity extends BaseActivity {

    private Handler mHandler;

    @Override
    protected int initLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 判断是否是第一次开启应用
//        boolean isFirstOpen = Config.getInstance().getBoolean(Constants.FIRST_OPEN, true);
        // 如果是第一次启动，则先进入功能引导页
       /* if (isFirstOpen) {
            Config.getInstance().setBoolean(Constants.FIRST_OPEN, false);
            Intent intent = new Intent(this, GuideActivity.class);
            startActivity(intent);
            finish();
            return;
        }*/
        mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {

            @Override
            public void run() {
                enterHomeActivity();
            }
        }, 2000);
    }

    private void enterHomeActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mHandler != null){
            mHandler.removeCallbacksAndMessages(null);
        }
    }
}

