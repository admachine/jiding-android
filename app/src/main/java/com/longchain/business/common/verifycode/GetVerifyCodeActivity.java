package com.longchain.business.common.verifycode;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.longchain.R;
import com.longchain.common.base.BaseActivity;
import com.longchain.business.common.iview.IGetVerifyCode;
import com.longchain.manager.AccountManager;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by 28367 on 2018/3/17.
 */

public class GetVerifyCodeActivity extends BaseActivity implements IGetVerifyCode {
    @BindView(R.id.tv_input_id)
    EditText mAccountEt;
    @BindView(R.id.clear_btn)
    ImageView mClearBtn;
    @BindView(R.id.next_btn)
    Button mNextBtn;
    @BindView(R.id.title_tv)
    TextView mTitleTv;
    @BindView(R.id.text_next)
    TextView mTextNext;
    @BindView(R.id.text_protocol)
    TextView mTextProtocol;
    private boolean mIsRegister = true;
    private GetVerifyCodePresenter mGetVerifyCodePresenter;

    @Override
    protected int initLayoutId() {
        return R.layout.activity_regster;
    }

    @Override
    protected void initView() {
        if (!mIsRegister) {
            mTitleTv.setText(getString(R.string.input_phone_number));
            mTextNext.setVisibility(View.GONE);
            mTextProtocol.setVisibility(View.GONE);
        }
        mAccountEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int before, int count) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                if (charSequence != null) {
                    if (charSequence.length() > 0) {
                        mClearBtn.setVisibility(View.VISIBLE);
                    } else {
                        mClearBtn.setVisibility(View.GONE);
                    }

                    if (charSequence.length() >= 11) {
                        mNextBtn.setEnabled(true);
                        mNextBtn.setTextColor(getResources().getColor(R.color.text_color));
                    } else {
                        mNextBtn.setTextColor(getResources().getColor(R.color.white));
                        mNextBtn.setEnabled(false);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    protected void initData() {
        mGetVerifyCodePresenter = new GetVerifyCodePresenter(this);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            mIsRegister = bundle.getBoolean("register", true);
        }
    }

    @OnClick(R.id.clear_btn)
    public void onClearClicked(View view) {
        mAccountEt.setText("");
    }

    @OnClick(R.id.next_btn)
    public void onNextBtnClicked(View view) {
        //TODO 发送验证码

        AccountManager.getInstance().sms(mAccountEt.getText().toString(),null);
        mGetVerifyCodePresenter.getVerifyCode(mAccountEt.getText().toString());
        Bundle bundle = new Bundle();
        bundle.putString("phoneNum", mAccountEt.getText().toString());
        Intent intent = new Intent(this, VerifyCodeActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
    }

    @Override
    public void getVerifyCode(String num) {

    }
}
