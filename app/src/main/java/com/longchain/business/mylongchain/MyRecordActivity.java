package com.longchain.business.mylongchain;

import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.longchain.R;
import com.longchain.common.base.BaseActivity;
import com.hh.base.wedgit.TitleBar;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by 28367 on 2018/4/1.
 */

public class MyRecordActivity extends BaseActivity {

    @BindView(R.id.container_layout)
    FrameLayout mContainerLayout;
    @BindView(R.id.title)
    TitleBar mTitle;
    @BindView(R.id.income_tv)
    TextView mIncomeTv;
    @BindView(R.id.cash_tv)
    TextView mCashTv;
    private static final int TAB_COUNT = 2;
    private Fragment[] mFragments = new Fragment[TAB_COUNT];

    @Override
    protected int initLayoutId() {
        return R.layout.activity_cash_list;
    }

    @Override
    protected void initView() {
        mTitle.setTitle("历史记录");
        selectItem(0);
    }

    @Override
    protected void initData() {

    }

    private void selectItem(int index) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        hideAllFragment(fragmentTransaction);
        switch (index) {
            case 0:
                mIncomeTv.setTextColor(Color.parseColor("#5095f4"));
                mCashTv.setTextColor(Color.parseColor("#606060"));
                if (mFragments[index] == null) {
                    mFragments[index] = new IncomeListFragment();
                    fragmentTransaction.add(R.id.container_layout, mFragments[index]);
                } else {
                    fragmentTransaction.show(mFragments[index]);
                }
                break;
            case 1:
                mIncomeTv.setTextColor(Color.parseColor("#606060"));
                mCashTv.setTextColor(Color.parseColor("#5095f4"));
                if (mFragments[index] == null) {
                    mFragments[index] = new CashListFragement();
                    fragmentTransaction.add(R.id.container_layout, mFragments[index]);
                } else {
                    fragmentTransaction.show(mFragments[index]);
                }
                break;
            default:
                break;
        }
        fragmentTransaction.commitAllowingStateLoss();
    }

    private void hideAllFragment(FragmentTransaction fragmentTransaction) {
        for (Fragment fragment : mFragments) {
            if (fragment != null) {
                fragmentTransaction.hide(fragment);
            }
        }
    }

    @OnClick({R.id.income_tv, R.id.cash_tv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.income_tv:
                selectItem(0);
                break;
            case R.id.cash_tv:
                selectItem(1);
                break;
        }
    }
}
