package com.longchain.business.mylongchain;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.longchain.R;
import com.longchain.business.common.setpwd.SetPwdActivity;
import com.longchain.business.personal.PersonalItem;
import com.longchain.business.personal.PersonalSection;
import com.longchain.common.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by 28367 on 2018/4/1.
 */

public class IncomeListFragment extends BaseFragment {
    @BindView(R.id.income_tv)
    TextView mIncomeTv;
    @BindView(R.id.money_tv)
    TextView mMoneyTv;
    @BindView(R.id.money_list)
    RecyclerView mMoneyList;

    @Override
    protected int setLayoutResourceID() {
        return R.layout.cash_record_fragment;
    }

    @Override
    protected void setUpView() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        mMoneyList.setLayoutManager(layoutManager);
        IncomeAdapter incomeAdapter = new IncomeAdapter(R.layout.layout_cash_item,R.layout.income_list_header,getIncomeList());
        mMoneyList.setAdapter(incomeAdapter);
        incomeAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                startActivity(new Intent(getActivity(),RecordDetailActivity.class));
            }
        });
    }

    @Override
    protected void setUpData() {

    }

    private List<IncomeSection> getIncomeList() {
        List<IncomeSection> list = new ArrayList<>();
        list.add(new IncomeSection(true, "本月"));
        list.add(new IncomeSection(new IncomeBean()));
        list.add(new IncomeSection(new IncomeBean()));
        list.add(new IncomeSection(new IncomeBean()));
        list.add(new IncomeSection(new IncomeBean()));
        list.add(new IncomeSection(true, "1月"));
        list.add(new IncomeSection(new IncomeBean()));
        list.add(new IncomeSection(new IncomeBean()));
        list.add(new IncomeSection(new IncomeBean()));
        list.add(new IncomeSection(new IncomeBean()));
        list.add(new IncomeSection(new IncomeBean()));
        list.add(new IncomeSection(true, "2017年12月"));
        list.add(new IncomeSection(new IncomeBean()));
        list.add(new IncomeSection(new IncomeBean()));
        list.add(new IncomeSection(new IncomeBean()));
        list.add(new IncomeSection(true, "2017年12月"));
        list.add(new IncomeSection(new IncomeBean()));
        list.add(new IncomeSection(new IncomeBean()));
        return list;
    }
}
