package com.longchain.business.mylongchain;

import com.chad.library.adapter.base.entity.SectionEntity;

/**
 * Created by 28367 on 2018/4/1.
 */

public class IncomeSection extends SectionEntity<IncomeBean> {
    IncomeBean incomeBean;

    public IncomeBean getIncomeBean() {
        return incomeBean;
    }

    public String getHeader() {
        return header;
    }

    String header;
    public IncomeSection(boolean isHeader, String header) {
        super(isHeader, header);
        this.header = header;
    }

    public IncomeSection(IncomeBean incomeBean) {
        super(incomeBean);
        this.incomeBean = incomeBean;
    }
}
