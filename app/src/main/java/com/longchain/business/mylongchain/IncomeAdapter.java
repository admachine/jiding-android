package com.longchain.business.mylongchain;

import com.chad.library.adapter.base.BaseSectionQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.longchain.R;

import java.util.List;

/**
 * Created by 28367 on 2018/4/1.
 */

public class IncomeAdapter extends BaseSectionQuickAdapter<IncomeSection,BaseViewHolder> {
    /**
     * Same as QuickAdapter#QuickAdapter(Context,int) but with
     * some initialization data.
     *
     * @param layoutResId      The layout resource id of each item.
     * @param sectionHeadResId The section head layout id for each item
     * @param data             A new list is created out of this one to avoid mutable list
     */
    public IncomeAdapter(int layoutResId, int sectionHeadResId, List<IncomeSection> data) {
        super(layoutResId, sectionHeadResId, data);
    }

    @Override
    protected void convertHead(BaseViewHolder helper, IncomeSection item) {
        helper.setText(R.id.section_title,item.getHeader());
    }

    @Override
    protected void convert(BaseViewHolder helper, IncomeSection item) {

    }
}
