package com.longchain.business.mylongchain;

import android.widget.TextView;

import com.longchain.R;
import com.longchain.common.base.BaseActivity;
import com.hh.base.wedgit.TitleBar;

import butterknife.BindView;

/**
 * Created by 28367 on 2018/4/1.
 */

public class RecordDetailActivity extends BaseActivity {
    @BindView(R.id.title)
    TitleBar mTitle;
    @BindView(R.id.create_time)
    TextView mCreateTime;
    @BindView(R.id.update_time)
    TextView mUpdateTime;
    @BindView(R.id.address_tv)
    TextView mAddressTv;

    @Override
    protected int initLayoutId() {
        return R.layout.activity_record_detail;
    }

    @Override
    protected void initView() {
        mTitle.setTitle("提取详情");
    }

    @Override
    protected void initData() {

    }

}
