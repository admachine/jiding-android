package com.longchain.business;


import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.longchain.R;
import com.longchain.business.add.AddContentActivity;
import com.longchain.business.home.HomeFragment;
import com.longchain.business.personal.PersonalFragment;
import com.longchain.business.search.SearchActivity;
import com.longchain.common.base.BaseActivity;

import butterknife.BindView;
import butterknife.OnClick;


public class HomeActivity extends BaseActivity {

    @BindView(R.id.home_icon)
    ImageView mHomeIcon;
    @BindView(R.id.personal_icon)
    ImageView mPersonalIcon;
    @BindView(R.id.mine_tv)
    TextView mMineTv;
    @BindView(R.id.home_tv)
    TextView mHomeTv;
    private static final int TAB_COUNT = 2;
    private Fragment[] mFragments = new Fragment[TAB_COUNT];

    @Override
    protected int initLayoutId() {
        return R.layout.activity_home;
    }

    @Override
    protected void initView() {
        selectItem(0);
    }

    @Override
    protected void initData() {

    }

    @OnClick({ R.id.home_tab, R.id.add_content, R.id.personal_tab})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            /*case R.id.search_btn:
                startActivity(new Intent(this, SearchActivity.class));
                break;
            case R.id.gift_btn:
                break;*/
            case R.id.home_tab:
                selectItem(0);
                break;
            case R.id.add_content:
                startActivity(new Intent(this, AddContentActivity.class));
                break;
            case R.id.personal_tab:
                selectItem(1);
                break;
        }
    }

    private void selectItem(int index) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        hideAllFragment(fragmentTransaction);
        switch (index) {
            case 0:
                mHomeTv.setTextColor(getResources().getColor(R.color.nav_bar_text_color_select));
                mMineTv.setTextColor(getResources().getColor(R.color.nav_bar_text_color));
                mPersonalIcon.setImageResource(R.drawable.icon_my);
                mHomeIcon.setImageResource(R.drawable.icon_homepage_click);
                if (mFragments[index] == null) {
                    mFragments[index] = new HomeFragment();
                    fragmentTransaction.add(R.id.fragment_container, mFragments[index]);
                } else {
                    fragmentTransaction.show(mFragments[index]);
                }
                break;
            case 1:
                mHomeTv.setTextColor(getResources().getColor(R.color.nav_bar_text_color));
                mMineTv.setTextColor(getResources().getColor(R.color.nav_bar_text_color_select));
                mPersonalIcon.setImageResource(R.drawable.icon_my_click);
                mHomeIcon.setImageResource(R.drawable.icon_homepage);
                if (mFragments[index] == null) {
                    mFragments[index] = new PersonalFragment();
                    fragmentTransaction.add(R.id.fragment_container, mFragments[index]);
                } else {
                    fragmentTransaction.show(mFragments[index]);
                }
                break;
            default:
                break;
        }
        fragmentTransaction.commitAllowingStateLoss();
    }

    private void hideAllFragment(FragmentTransaction fragmentTransaction) {
        for (Fragment fragment : mFragments) {
            if (fragment != null) {
                fragmentTransaction.hide(fragment);
            }
        }
    }
}
