package com.longchain.business.personal;

import com.longchain.R;
import com.longchain.common.base.BaseActivity;
import com.longchain.manager.DeviceManager;
import com.tsy.sdk.myokhttp.response.JsonResponseHandler;

import org.json.JSONArray;

/**
 * Created by Administrator on 2018/4/5.
 */

public class DeviceManagerActivity extends BaseActivity{
    @Override
    protected int initLayoutId() {
        return R.layout.activity_device_manager;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {

        DeviceManager.getInstance().getDiviceList(new JsonResponseHandler() {
            @Override
            public void onFailure(int i, String s) {

            }

            @Override
            public void onSuccess(int statusCode, JSONArray response) {
                super.onSuccess(statusCode, response);
            }
        });
    }
}
