package com.longchain.business.personal;

import android.support.v7.widget.RecyclerView;

import com.chad.library.adapter.base.BaseSectionQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.longchain.R;

import java.util.List;

/**
 * Created by 28367 on 2018/4/1.
 */

public class PersonalAdapter extends BaseSectionQuickAdapter<PersonalSection, BaseViewHolder> {
    /**
     * Same as QuickAdapter#QuickAdapter(Context,int) but with
     * some initialization data.
     *
     * @param layoutResId      The layout resource id of each item.
     * @param sectionHeadResId The section head layout id for each item
     * @param data             A new list is created out of this one to avoid mutable list
     */
    public PersonalAdapter(int layoutResId, int sectionHeadResId, List data) {
        super(layoutResId, sectionHeadResId, data);
    }

    @Override
    protected void convertHead(BaseViewHolder helper, PersonalSection item) {

    }

    @Override
    protected void convert(BaseViewHolder helper, PersonalSection item) {
        helper.setText(R.id.item_name, item.getPersonalItem().getName());
        helper.setImageResource(R.id.item_icon, item.getPersonalItem().getIcon());
    }

    @Override
    public int getItemCount() {
        return super.getItemCount();
    }
}
