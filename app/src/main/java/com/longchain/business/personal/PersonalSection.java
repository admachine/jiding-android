package com.longchain.business.personal;

import com.chad.library.adapter.base.entity.SectionEntity;

/**
 * Created by 28367 on 2018/4/1.
 */

public class PersonalSection extends SectionEntity<PersonalItem> {
    public PersonalItem getPersonalItem() {
        return mPersonalItem;
    }

    private PersonalItem mPersonalItem;
    public PersonalSection(boolean isHeader, String header) {
        super(isHeader, header);
    }

    public PersonalSection(PersonalItem personalItem) {
        super(personalItem);
        mPersonalItem = personalItem;
    }
}
