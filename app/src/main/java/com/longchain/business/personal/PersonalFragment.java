package com.longchain.business.personal;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.longchain.R;
import com.longchain.business.cloudplayer.CloudPlayerActivity;
import com.longchain.business.help.HelpAndFeedbackActivity;
import com.longchain.business.mylongchain.MyChainActivity;
import com.longchain.common.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by 28367 on 2018/3/19.
 */

public class PersonalFragment extends BaseFragment {
    @BindView(R.id.content_list)
    RecyclerView mContentList;

    @Override
    protected int setLayoutResourceID() {
        return R.layout.personal_fragment;
    }

    @Override
    protected void setUpView() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        mContentList.setLayoutManager(layoutManager);
        final PersonalAdapter personalAdapter = new PersonalAdapter(R.layout.personal_item, R.layout.personal_header, getPersonList());
        mContentList.setAdapter(personalAdapter);
        personalAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                PersonalSection personalSection = personalAdapter.getData().get(position);
                PersonalItem personalItem = personalSection.getPersonalItem();
                startActivity(new Intent(getActivity(), personalItem.getActivityClass()));
            }
        });
    }

    @Override
    protected void setUpData() {
    }

    private List<PersonalSection> getPersonList() {
        List<PersonalSection> list = new ArrayList<>();
        list.add(new PersonalSection(new PersonalItem(MyChainActivity.class, "我的LC", R.drawable.my_ic_chain)));
        list.add(new PersonalSection(new PersonalItem(MessageCenterActivity.class, "消息中心", R.drawable.my_ic_new)));
        list.add(new PersonalSection(true, "Section 1"));
        list.add(new PersonalSection(new PersonalItem(TransportListActivity.class, "传输列表", R.drawable.my_ic_transfer)));
        list.add(new PersonalSection(new PersonalItem(CloudAddActivity.class, "云添加列表", R.drawable.my_ic_cloudlist)));
        list.add(new PersonalSection(true, "Section 2"));
        list.add(new PersonalSection(new PersonalItem(CloudPlayerActivity.class, "云多媒体", R.drawable.my_ic_multimedia)));
        list.add(new PersonalSection(new PersonalItem(AutoBackUpActivity.class, "自动备份", R.drawable.my_ic_backup)));
        list.add(new PersonalSection(true, "Section 3"));
        list.add(new PersonalSection(new PersonalItem(DeviceManagerActivity.class, "设备管理", R.drawable.my_ic_equipment)));
        list.add(new PersonalSection(new PersonalItem(SettingActivity.class, "APP设置", R.drawable.my_ic_install)));
        list.add(new PersonalSection(new PersonalItem(HelpAndFeedbackActivity.class, "帮助与反馈", R.drawable.my_ic_helpfeedback)));
        list.add(new PersonalSection(new PersonalItem(AboutUsActivity.class, "关于我们", R.drawable.my_ic_about)));
        list.add(new PersonalSection(true, "Section 4"));
        return list;
    }

}
