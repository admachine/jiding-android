package com.longchain.business.personal;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.longchain.R;
import com.longchain.common.base.BaseActivity;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Administrator on 2018/4/5.
 */

public class TransportListActivity extends BaseActivity {

    @BindView(R.id.un_upload_tab)
    TextView unUploadTab;

    @BindView(R.id.all_file_tab)
    TextView allFileTab;

    @BindView(R.id.un_upload_tab_icon)
    ImageView unUploadTabIcon;

    @BindView(R.id.all_file_tab_icon)
    ImageView allFileTabIcon;

    @Override
    protected int initLayoutId() {
        return R.layout.activity_tranport_list;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {

    }


    @OnClick({R.id.un_upload_tab, R.id.all_file_tab})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.un_upload_tab:
                unUploadTabIcon.setVisibility(View.VISIBLE);
                allFileTabIcon.setVisibility(View.GONE);
                unUploadTab.setTextColor(0xff5095f4);
                allFileTab.setTextColor(0xff606060);
                break;
            case  R.id.all_file_tab:
                unUploadTabIcon.setVisibility(View.GONE);
                allFileTabIcon.setVisibility(View.VISIBLE);
                unUploadTab.setTextColor(0xff606060);
                allFileTab.setTextColor(0xff5095f4);
                break;
        }
    }
}
