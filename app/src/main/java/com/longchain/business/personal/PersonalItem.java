package com.longchain.business.personal;

/**
 * Created by 28367 on 2018/4/1.
 */

public class PersonalItem {

    public Class getActivityClass() {
        return mClass;
    }

    private Class mClass;

    public String getName() {
        return mName;
    }

    public int getIcon() {
        return mIcon;
    }

    private String mName;
    private int mIcon;

    public PersonalItem(Class<?> clazz, String itemName, int icon) {
        mClass = clazz;
        mName = itemName;
        mIcon = icon;
    }
}
