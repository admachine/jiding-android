package com.longchain.business.search;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.hh.base.MyUtils;
import com.longchain.R;
import com.longchain.manager.FileManager;
import com.longchain.manager.entity.LongChainFile;
import com.longchain.manager.entity.SeverFileEntity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/4/22.
 */

public class FileAdapter extends RecyclerView.Adapter<FileAdapter.ViewHolder> implements OnGetSelectImages {
    private final LayoutInflater mLayoutInflater;
    private final Context mContext;
    public List<SeverFileEntity> mDatas;
    public ArrayList<SeverFileEntity> selectImages;

    public FileAdapter(Context context, List<SeverFileEntity> title, OnItemClick l) {
        mDatas = title;
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
        mOnItemClick = l;
        selectImages = new ArrayList<>();
    }

    public void setmDatas(List<SeverFileEntity> mDatas) {
        this.mDatas = mDatas;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder viewHolder = new ViewHolder(mLayoutInflater.inflate(R.layout.file_list_item, parent, false));
        viewHolder.setmOnItemClick(mOnItemClick);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SeverFileEntity file = mDatas.get(position);
        holder.mNameView.setText(mDatas.get(position).getName());
        holder.mTimeView.setText(mDatas.get(position).getTime());

        RequestOptions options = new RequestOptions();
        options.placeholder(R.drawable.icon_pic);
        Glide.with(mContext)
                .asBitmap()
                .load(MyUtils.HOST+file.getThumbnail())
                .apply(options)
                .into(holder.mIconView);
    }

    @Override
    public int getItemCount() {
        return mDatas == null ? 0 : mDatas.size();
    }

    @Override
    public List<SeverFileEntity> getSelectedImages() {
        if (selectImages == null) {
            selectImages = new ArrayList<>();
        }
        return selectImages;
    }

    public  class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_name)
        TextView mNameView;

        @BindView(R.id.time)
        TextView mTimeView;

        @BindView(R.id.item_select)
        ImageView mSelectView;

        @BindView(R.id.item_icon)
        ImageView mIconView;

        OnItemClick mOnItemClick;

        public void setmOnItemClick(OnItemClick mOnItemClick) {
            this.mOnItemClick = mOnItemClick;
        }

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnItemClick != null) {
                        mOnItemClick.onItemClick(getAdapterPosition());
                    }
                }
            });
            mSelectView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mSelectView.isSelected()) {
                        mSelectView.setSelected(false);
                        selectImages.remove(mDatas.get(getAdapterPosition()));
                    } else {
                        mSelectView.setSelected(true);
                        selectImages.add(mDatas.get(getAdapterPosition()));
                    }
                }
            });
        }
    }

    public OnItemClick mOnItemClick;

    public void setmOnItemClick(OnItemClick mOnItemClick) {
        this.mOnItemClick = mOnItemClick;
    }

    public interface OnItemClick {
        void onItemClick(int position);
    }
}