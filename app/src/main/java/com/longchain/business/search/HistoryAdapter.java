package com.longchain.business.search;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hh.base.log.SLog;
import com.longchain.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/4/22.
 */

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.NormalTextViewHolder> {
    private final LayoutInflater mLayoutInflater;
    private final Context mContext;
    private List<String> mTitles;

    public HistoryAdapter(Context context,List<String> title ,OnHistoryClick l) {
        mTitles = title;
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
        mOnHistoryClick = l;
    }

    public void setmTitles(List<String> mTitles) {
        this.mTitles = mTitles;
        notifyDataSetChanged();
    }

    @Override
    public NormalTextViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        NormalTextViewHolder viewHolder = new NormalTextViewHolder(mLayoutInflater.inflate(R.layout.search_history_item, parent, false));
        viewHolder.setmOnHistoryClick(mOnHistoryClick);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(NormalTextViewHolder holder, int position) {
        holder.mTextView.setText(mTitles.get(position));
    }

    @Override
    public int getItemCount() {
        return mTitles == null ? 0 : mTitles.size();
    }

    public static class NormalTextViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_name)
        TextView mTextView;

        @BindView(R.id.item_delete)
        ImageView mDeleteView;
        OnHistoryClick mOnHistoryClick;

        public void setmOnHistoryClick(OnHistoryClick mOnHistoryClick) {
            this.mOnHistoryClick = mOnHistoryClick;
        }

        NormalTextViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            mDeleteView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   if (mOnHistoryClick!=null){
                       mOnHistoryClick.onDeleteClick(getAdapterPosition());
                   }
                }
            });

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnHistoryClick !=null){
                        mOnHistoryClick.onItemClick(getAdapterPosition());
                    }
                }
            });
        }
    }

    public OnHistoryClick mOnHistoryClick;

    public void setmOnHistoryClick(OnHistoryClick mOnHistoryClick) {
        this.mOnHistoryClick = mOnHistoryClick;
    }

    public interface OnHistoryClick{
        void onDeleteClick(int position);
        void onItemClick(int position);
    }
}