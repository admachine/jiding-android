package com.longchain.business.search;

import com.longchain.R;
import com.longchain.manager.FileManager;
import com.longchain.manager.entity.LongChainFile;
import com.longchain.manager.entity.SeverFileEntity;

import java.util.List;

/**
 * Created by Administrator on 2018/4/20.
 */

public class SearchVideoFragment extends BaseSearchFragment {
    private static final String TAG = "SearchAllFragment";

    @Override
    protected void setUpView() {
        super.setUpView();
        mEmptyImage.setImageResource(R.drawable.list_img_video_nothing);
    }

    @Override
    public void updateData(List<SeverFileEntity> mDatas) {
       super.updateData(mDatas);
    }

    @Override
    public String getFileType() {
        return FileManager.STATUS_VIDEO;
    }
}
