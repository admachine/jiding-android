package com.longchain.business.search;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.hh.base.MyUtils;
import com.hh.base.log.SLog;
import com.hh.base.util.JsonUtils;
import com.hh.base.util.SPUtils;
import com.longchain.common.base.LongchainApplication;
import com.longchain.manager.AccountManager;
import com.tsy.sdk.myokhttp.response.JsonResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2018/4/22.
 */

public class SearchHistoryController {
    private static final String TAG = "SearchHistoryController";

    public static final String SEARCH_KEY = "search_key";
    public static final int MAX_SERACH_KEY_NUM = 5;

    public static void insertSearchKeyToSP(String key) {
        List<String> jsonObjectList = getHistorySearchKey();
        try {

           if(jsonObjectList.contains(key)){
               jsonObjectList.remove(key);
           }
            if (jsonObjectList.size() >= MAX_SERACH_KEY_NUM) {
               jsonObjectList.remove(jsonObjectList.size()-1);
            }
            jsonObjectList.add(0, key);
            JSONArray result = new JSONArray();
            for (int i = 0; i < jsonObjectList.size(); i++) {
                result.put(jsonObjectList.get(i));
            }
            SPUtils.common().put(SEARCH_KEY,result.toString());
        } catch (Exception e) {
            SLog.e(TAG, "插入历史搜索key时,json解析出错,原因:" + e.getMessage());
        }
    }

    public static List<String> getHistorySearchKey() {
        String histKey = SPUtils.common().get(SEARCH_KEY, "");
        JSONArray jsonArray = null;
        List<String> jsonObjectList =null ;
        try {
            if (!TextUtils.isEmpty(histKey)) {
                jsonArray = new JSONArray(histKey);
            } else {
                jsonArray = new JSONArray();
            }
            jsonObjectList = JsonUtils.parserToList(jsonArray);

        } catch (Exception e) {
            SLog.e(TAG, "获取历史搜索key时,json解析出错,原因:" + e.getMessage());
        }
        return jsonObjectList;
    }


    public static void clearAllHistory(){
        SPUtils.common().put(SEARCH_KEY,"");
    }
    public static void deleteHistory(String key){
        List<String> jsonObjectList = getHistorySearchKey();
        try {

            if(jsonObjectList.contains(key)){
                jsonObjectList.remove(key);
            }

            JSONArray result = new JSONArray();
            for (int i = 0; i < jsonObjectList.size(); i++) {
                result.put(jsonObjectList.get(i));
            }
            SPUtils.common().put(SEARCH_KEY,result.toString());
        } catch (Exception e) {
            SLog.e(TAG, "删除历史搜索key时,json解析出错,原因:" + e.getMessage());
        }
    }

    public static void search(Context context,String key){

        /*Map params = new HashMap<>();
        int page = 1;
        params.put("name", key);
        params.put("page", String.valueOf(page));
        params.put("pageSize", String.valueOf(20));
        LongchainApplication.getInstance().getMyOkHttp().get().url("/search/").params(params).tag(context).addHeader("Authorization",AccountManager.token).enqueue(new JsonResponseHandler() {

            @Override
            public void onSuccess(int statusCode, JSONObject response) {
                super.onSuccess(statusCode, response);
                Log.d(TAG,"search success :"+response.toString());

            }

            @Override
            public void onFailure(int statusCode, String error_msg) {

            }
        });*/
    }

}
