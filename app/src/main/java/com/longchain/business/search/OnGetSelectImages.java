package com.longchain.business.search;

import com.longchain.manager.entity.LongChainFile;
import com.longchain.manager.entity.SeverFileEntity;

import java.util.List;

/**
 * Created by dh on 2018/4/29.
 */

public interface OnGetSelectImages {
    public List<SeverFileEntity> getSelectedImages();
}
