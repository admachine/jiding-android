package com.longchain.business.files;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;

import com.hh.base.dialog.SubmitMessageDialog;
import com.hh.base.log.SLog;
import com.hh.base.util.UIUtil;
import com.hjy.http.download.DownloadManager;
import com.hjy.http.download.FileDownloadTask;
import com.hjy.http.download.listener.OnDownloadingListener;
import com.longchain.R;
import com.longchain.common.widget.BottomFileOperationLayout;
import com.longchain.manager.FileManager;
import com.longchain.manager.entity.LongChainFile;
import com.luck.picture.lib.PicturePreviewActivity;
import com.luck.picture.lib.PictureVideoPlayActivity;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.config.PictureSelectionConfig;
import com.luck.picture.lib.decoration.GridSpacingItemDecoration;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.observable.ImagesObservable;
import com.luck.picture.lib.tools.ScreenUtils;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.OnClick;

/**
 * Created by Administrator on 2018/4/9.
 */

public class ImageListActivity extends FileListActivity{

    private PictureImageGridAdapter adapter;
    PictureSelectionConfig  config;
    protected List<LocalMedia> dataList;

    @Override
    protected void initView() {

        mTitleBar.setTitle("照片");
        config = PictureSelectionConfig.getCleanInstance();

        dataList = new ArrayList<>();

        LocalMedia localMedia = new LocalMedia();
        LocalMedia localMedia1 = new LocalMedia();
        LocalMedia localMedia2 = new LocalMedia();
        LocalMedia localMedia3 = new LocalMedia();
        LocalMedia localMedia4 = new LocalMedia();
        localMedia1.setPath("https://timgsa.baidu.com/timg?image&quality=80&size=b10000_10000&sec=1524799675&di=8acaebb8186111aff84ed56c8ea76c05&src=http://img7.aili.com/201601/28/1453953020_87561200.jpg");
        localMedia4.setPath("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1524809735052&di=028f2de92787d99f874864f847b62f9b&imgtype=0&src=http%3A%2F%2Fwww.des13.cc%2Fstar%2Fmedia%2Fk2%2Fitems%2Fcache%2F1ea804dbf6a45ba977293c237ecc1b08_XL.jpg");
        localMedia3.setPath("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1524809681114&di=d92ea04d708dd8c89e0f5d68b1a8f517&imgtype=0&src=http%3A%2F%2Fpic.yesky.com%2FuploadImages%2F2015%2F216%2F41%2F9Y2X688BGX6A.jpg");
        localMedia2.setPath("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1524809636083&di=f7c1cb62dd8d222c331761b1727d930b&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fimgad%2Fpic%2Fitem%2F060828381f30e92483987f3746086e061d95f7fc.jpg");
        localMedia.setPath("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1524662125958&di=9bd5e9258741cb8e72a45d8824a170cc&imgtype=0&src=http%3A%2F%2Fi1.mopimg.cn%2Fimg%2Fdzh%2F2015-07%2F414%2F20150717120946794.jpg");
        dataList.add(localMedia);
        dataList.add(localMedia1);
        dataList.add(localMedia2);
        dataList.add(localMedia3);
        dataList.add(localMedia4);
        mrecylerView.setHasFixedSize(true);
        mrecylerView.addItemDecoration(new GridSpacingItemDecoration(4,
                ScreenUtils.dip2px(this, 5), true));
        mrecylerView.setLayoutManager(new GridLayoutManager(this, 4));
        adapter = new PictureImageGridAdapter(mContext, config);
        adapter.setOnPhotoSelectChangedListener(new PictureImageGridAdapter.OnPhotoSelectChangedListener() {
            @Override
            public void onTakePhoto() {

            }

            @Override
            public void onChange(List<LocalMedia> selectImages) {

            }

            @Override
            public void onPictureClick(LocalMedia media, int position) {
                List<LocalMedia> images = adapter.getImages();
                startPreview(images, position);
            }
        });
        mBottomLayout.setOnOperationListener(new BottomFileOperationLayout.OnOperation() {
            @Override
            public void onDeleteClick() {

                final List<LocalMedia> selectedImages = adapter.getSelectedImages();
                if (selectedImages != null && selectedImages.size() > 0) {
                    UIUtil.showAlertDialog(ImageListActivity.this, "确定删除文件？", "确定删除后文件不可恢复", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            for (LocalMedia localMedia : selectedImages) {
                                dataList.remove(localMedia);
                            }
                            adapter.bindImagesData(dataList);
                        }
                    });
                } else {
                    UIUtil.showToast("请先选中要删除的文件");
                }


            }

            @Override
            public void onDownLoadClick() {
                UIUtil.showToast("已保存到相册");
            }

            @Override
            public void onEditTagClick() {

            }
        });
        mrecylerView.setAdapter(adapter);
        adapter.bindImagesData(dataList);
//        adapter.bindSelectImages(dataList);

    }
    public void startPreview(List<LocalMedia> previewImages, int position) {
        LocalMedia media = previewImages.get(position);
        String pictureType = media.getPictureType();
        Bundle bundle = new Bundle();
        List<LocalMedia> result = new ArrayList<>();
        int mediaType = PictureMimeType.isPictureType(pictureType);
        switch (mediaType) {
            case PictureConfig.TYPE_IMAGE:
                // image
                List<LocalMedia> selectedImages = adapter.getSelectedImages();
                ImagesObservable.getInstance().saveLocalMedia(previewImages);
                bundle.putSerializable(PictureConfig.EXTRA_SELECT_LIST, (Serializable) selectedImages);
                bundle.putInt(PictureConfig.EXTRA_POSITION, position);
                Intent intent = new Intent();
                intent.setClass(this, PicturePreviewActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
                overridePendingTransition(com.luck.picture.lib.R.anim.a5, 0);
                break;
            case PictureConfig.TYPE_VIDEO:
                // video
                if (config.selectionMode == PictureConfig.SINGLE) {
                    result.add(media);
//                    onResult(result);
                } else {
                    bundle.putString("video_path", media.getPath());
                    Intent intent1 = new Intent();
                    intent1.setClass(this, PictureVideoPlayActivity.class);
                    intent1.putExtras(bundle);
                    startActivity(intent1);
                }
                break;
            case PictureConfig.TYPE_AUDIO:
                // audio
                if (config.selectionMode == PictureConfig.SINGLE) {
                    result.add(media);
//                    onResult(result);
                } else {
//                    audioDialog(media.getPath());
                }
                break;
        }
    }


   /* @OnClick({ R.id.download_layout, R.id.delete_layout,R.id.edit_layout})
    public void onViewClicked(View view) {

        switch (view.getId()) {
            case R.id.download_layout:
                DownloadManager.getInstance(this).downloadFile(PictureMimeType.ofAll(), "1.jpg", "http://myzexabox.6655.la:8020/zbx" +
                        "/file/download", new OnDownloadingListener() {
                    @Override
                    public void onDownloadFailed(FileDownloadTask task, int errorType, String msg) {

                        SLog.e(TAG,"download faild"+errorType+msg);
                    }

                    @Override
                    public void onDownloadSucc(FileDownloadTask task, File outFile) {
                        SLog.d(TAG,"download success!");

                    }
                });
                break;

            case R.id.delete_layout:
                List<LocalMedia> selectedImages= adapter.getSelectedImages();
                if (selectedImages!=null && selectedImages.size()>0){
                    for (LocalMedia localMedia:selectedImages){
                        dataList.remove(localMedia);
                    }
                    adapter.bindImagesData(dataList);
                }else {
                    UIUtil.showToast("请先选中要删除的文件");
                }

                break;
            case R.id.edit_layout:

                UIUtil.showSubmitDialog(this,"添加tag","",6, new SubmitMessageDialog.OnComfirmClickListener() {
                    @Override
                    public void onConfirmClick(View view, String content) {

                    }
                });
                break;

        }
    }
*/

    @Override
    protected String getFileType() {
        return FileManager.STATUS_IMAGE;
    }


}
