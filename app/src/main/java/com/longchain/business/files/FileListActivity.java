package com.longchain.business.files;

import android.support.v7.widget.RecyclerView;

import com.longchain.R;
import com.longchain.common.base.BaseActivity;
import com.longchain.common.widget.BottomFileOperationLayout;
import com.hh.base.wedgit.TitleBar;
import com.longchain.manager.FileManager;

import butterknife.BindView;

/**
 * Created by Administrator on 2018/4/25.
 */

public abstract class FileListActivity extends BaseActivity {
    public static final String TAG = "FileListActivity";
    @BindView(R.id.title_bar)
    TitleBar mTitleBar;
    @BindView(R.id.picture_recycler)
    RecyclerView mrecylerView;

    @BindView(R.id.bottom_layout)
    BottomFileOperationLayout mBottomLayout;
    @Override
    protected int initLayoutId() {
        return R.layout.activity_image_list;
    }

    @Override
    protected void initData() {
        FileManager.getInstance().getFileListByStatus(getFileType());
    }
    protected abstract String getFileType();
}
