package com.longchain.business.files;

import com.longchain.manager.FileManager;

/**
 * Created by Administrator on 2018/4/9.
 */

public class MusicListActivity extends FileListActivity{


    @Override
    protected void initView() {
        mTitleBar.setTitle("音乐");
    }



    @Override
    protected String getFileType() {
        return FileManager.STATUS_AUDIO;
    }


}
