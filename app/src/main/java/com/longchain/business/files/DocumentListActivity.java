package com.longchain.business.files;

import com.longchain.manager.FileManager;

/**
 * Created by Administrator on 2018/4/9.
 */

public class DocumentListActivity extends FileListActivity{


    @Override
    protected void initView() {
        mTitleBar.setTitle("文档");
    }



    @Override
    protected String getFileType() {
        return FileManager.STATUS_TEXT;
    }


}
