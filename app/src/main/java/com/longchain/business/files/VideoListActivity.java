package com.longchain.business.files;

import com.longchain.manager.FileManager;

/**
 * Created by Administrator on 2018/4/9.
 */

public class VideoListActivity extends FileListActivity{


    @Override
    protected void initView() {
        mTitleBar.setTitle("视频");
    }



    @Override
    protected String getFileType() {
        return FileManager.STATUS_VIDEO;
    }


}
