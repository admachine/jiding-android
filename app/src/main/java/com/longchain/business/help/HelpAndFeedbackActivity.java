package com.longchain.business.help;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;

import com.longchain.R;
import com.longchain.common.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by 28367 on 2018/4/1.
 */

public class HelpAndFeedbackActivity extends BaseActivity {
    @BindView(R.id.problem_list)
    RecyclerView mProblemList;
    @BindView(R.id.feedback_fl)
    LinearLayout mFeedbackFl;
    private List<String> mList = new ArrayList<>();

    @Override
    protected int initLayoutId() {
        return R.layout.activity_help_feedback;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mProblemList.setLayoutManager(linearLayoutManager);
        HelpAdapter helpAdapter = new HelpAdapter(R.layout.layout_help_item, mList);
        mProblemList.setAdapter(helpAdapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick(R.id.feedback_fl)
    public void onViewClicked() {
        startActivity(new Intent(this,HelpAndFeedbackActivity.class));
    }
}
