package com.longchain.business.help;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * Created by 28367 on 2018/4/1.
 */

public class HelpAdapter extends BaseQuickAdapter<String,BaseViewHolder>{
    public HelpAdapter(int layoutResId, @Nullable List<String> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {

    }

    @Override
    public int getItemCount() {
        return 10;
    }
}
