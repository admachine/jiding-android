package com.longchain.business.tag;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.hh.base.wedgit.TitleBar;
import com.longchain.R;
import com.longchain.common.base.BaseActivity;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Administrator on 2018/4/5.
 */

public class MoreTagsActivity extends BaseActivity {
    @BindView(R.id.tv_input_tag)
    EditText mAccountEt;
    @BindView(R.id.clear_btn)
    ImageView mClearBtn;
    @BindView(R.id.title_bar)
    TitleBar titleBar;

    @Override
    protected int initLayoutId() {
        return R.layout.activity_more_tags;
    }

    @Override
    protected void initView() {
        mAccountEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (TextUtils.isEmpty(charSequence.toString())) {
                    mClearBtn.setVisibility(View.GONE);
                } else {
                    mClearBtn.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {


            }
        });
        titleBar.setRightBtnVisiable(true);
    }

    @Override
    protected void initData() {

    }

    @OnClick(R.id.clear_btn)
    public void onClearAccount() {
        mAccountEt.setText("");
    }
}
