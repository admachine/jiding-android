package com.longchain.manager;

import android.util.Log;

import com.hh.base.MyUtils;
import com.hh.base.util.MD5Utils;
import com.longchain.common.base.LongchainApplication;
import com.hh.base.util.JsonUtils;
import com.tsy.sdk.myokhttp.response.JsonResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2018/4/8.
 */

public class AccountManager {
    private static final String TAG = "AccountManager";
   public static String loginurl ="/login";
    public static String registerurl ="/add_user";
    public static String sms ="/sms";
    private static AccountManager instance;
    public static AccountManager getInstance(){
        if (instance == null){
            synchronized (AccountManager.class){
                if (instance == null){
                    instance  = new AccountManager();
                }
            }
        }
        return instance;
    }
    private AccountManager(){

    }
    public static String token;
    public void login(String phone,String password,final JsonResponseHandler listener){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("account", phone);
            jsonObject.put("password", getEncryptionPassword(password));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        LongchainApplication.getInstance().getMyOkHttp().post().url(loginurl).jsonParams(jsonObject.toString()).tag(this).enqueue(new JsonResponseHandler() {

            @Override
            public void onSuccess(int statusCode, JSONObject response) {
                super.onSuccess(statusCode, response);
                Log.d(TAG,"login success :"+response.toString());
                JSONObject data = JsonUtils.getJSONObject(response,"data");
                JSONObject user = JsonUtils.getJSONObject(data,"user");
//                if (data!=null){
                    token = JsonUtils.getString(response,"token");
                    MyUtils.token = token;
//                }
                listener.onSuccess(statusCode,response);
            }

            @Override
            public void onFailure(int statusCode, String error_msg) {
                listener.onFailure(statusCode,error_msg);
            }
        });
    }

    public void sms(String phone,final JsonResponseHandler listener){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("phone", phone);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        LongchainApplication.getInstance().getMyOkHttp().post().url(sms).jsonParams(jsonObject.toString()).tag(this).enqueue(new JsonResponseHandler() {

            @Override
            public void onSuccess(int statusCode, JSONObject response) {
                super.onSuccess(statusCode, response);
                Log.d(TAG,"sms success :"+response.toString());
                JSONObject data = JsonUtils.getJSONObject(response,"data");
                if (data!=null){
                    token = JsonUtils.getString(data,"sms_token");
                    MyUtils.token = token;
                }
            }

            @Override
            public void onFailure(int statusCode, String error_msg) {

            }
        });
    }  public void register(String phone,String password,final JsonResponseHandler listener){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("sms_token", token);
            jsonObject.put("sms_code", "8888");
            JSONObject user = new JSONObject();

            user.put("phone", phone);
            user.put("password", getEncryptionPassword(password));
            jsonObject.put("user",user);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        LongchainApplication.getInstance().getMyOkHttp().post().url(registerurl).jsonParams(jsonObject.toString()).tag(this).enqueue(new JsonResponseHandler() {

            @Override
            public void onSuccess(int statusCode, JSONObject response) {
                super.onSuccess(statusCode, response);
                Log.d(TAG,"register success :"+response.toString());
                JSONObject data = JsonUtils.getJSONObject(response,"data");
                if (data!=null){
                    token = JsonUtils.getString(data,"token");
                    MyUtils.token = token;
                }
                listener.onSuccess(statusCode,response);
            }

            @Override
            public void onFailure(int statusCode, String error_msg) {

                listener.onFailure(statusCode,error_msg);
            }
        });
    }

    /***
     * salt = "e&VW$4#@Fk9)UC6b"   (常量, 类型:string)
     p1 = md5(salt)            (获取salt的32位MD5)
     p2 = md5(password)        (获取密码的32位MD5)
     p = md5(p1+p2)            (获取p1+p2的32位MD5)
     p为传入参数
     * @param password
     * @return
     */
   public String getEncryptionPassword(String password){

       String salt ="e&VW$4#@Fk9)UC6b";

       String p1 = MD5Utils.md5(salt);
       String p2 = MD5Utils.md5(password);
       String p = MD5Utils.md5(p1+p2);
       return p;
    }
}
