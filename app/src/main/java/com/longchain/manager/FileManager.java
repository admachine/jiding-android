package com.longchain.manager;

import android.util.Log;

import com.longchain.common.base.LongchainApplication;
import com.tsy.sdk.myokhttp.response.JsonResponseHandler;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2018/4/9.
 */

public class FileManager {
    private static final String TAG = "FileManager";
    private static final String imageList = "/file/list";
    private static final String fileList = "/file/list";
    private static final String fileCount = "/nas/overview";
    private static final String fileListstatus = "/file/list/";

    //(image,text,video,audio),
    public static final String STATUS_OTHER = "100";
    public static final String STATUS_ALL = "0";
    public static final String STATUS_IMAGE = "1";
    public static final String STATUS_VIDEO = "2";
    public static final String STATUS_TEXT = "3";
    public static final String STATUS_AUDIO = "4";

    private static final int PAGE_SIZE = 20;
    private static FileManager instance;
    public static FileManager getInstance(){
        if (instance == null){
            synchronized (FileManager.class){
                if (instance == null){
                    instance  = new FileManager();
                }
            }
        }
        return instance;
    }
    private FileManager(){

    }

    public void getFileCount(JsonResponseHandler responseHandler){
        LongchainApplication.getInstance().getMyOkHttp().get().url(fileCount).tag(this).addHeader("Authorization",AccountManager.token).enqueue(responseHandler);
    }
    public void getFileList(){
        Map params = new HashMap<>();
        int page = 1;
        params.put("page", String.valueOf(page));
        params.put("pageSize", String.valueOf(PAGE_SIZE));
        LongchainApplication.getInstance().getMyOkHttp().get().url(fileList).params(params).tag(this).addHeader("Authorization",AccountManager.token).enqueue(new JsonResponseHandler() {

            @Override
            public void onSuccess(int statusCode, JSONObject response) {
                super.onSuccess(statusCode, response);
                Log.d(TAG,"get image list success :"+response.toString());

            }

            @Override
            public void onFailure(int statusCode, String error_msg) {

            }
        });
    }
    public void getFileListByStatus(final String status){
        Map params = new HashMap<>();
        int page = 1;
        params.put("page", String.valueOf(page));
        params.put("status", status);
        params.put("pageSize", String.valueOf(PAGE_SIZE));
        LongchainApplication.getInstance().getMyOkHttp().get().url(fileList).params(params).tag(this).addHeader("Authorization",AccountManager.token).enqueue(new JsonResponseHandler() {

            @Override
            public void onSuccess(int statusCode, JSONObject response) {
                super.onSuccess(statusCode, response);
                Log.d(TAG,status+"get list success :"+response.toString());

            }

            @Override
            public void onFailure(int statusCode, String error_msg) {

            }

        });
    }
}
