package com.longchain.manager.entity;

import java.util.List;

/**
 * Created by Administrator on 2018/4/19.
 */

public class SearchResult {
    private List<LongChainFile> data;

    public List<LongChainFile> getData() {
        return data;
    }

    public void setData(List<LongChainFile> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "SearchResult{" +
                "data=" + data +
                '}';
    }
}
