package com.longchain.manager.entity;

import java.util.List;

/**
 * Created by Administrator on 2018/4/26.
 */

public class FileListResult {

    int code;
    FileListData data;
    String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public FileListData getData() {
        return data;
    }

    public void setData(FileListData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public class FileListData{

        private int pageIndex;
        private int pageSize;
        private int totalCount;

        private List<SeverFileEntity> files;

        public int getPageIndex() {
            return pageIndex;
        }

        public void setPageIndex(int pageIndex) {
            this.pageIndex = pageIndex;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(int totalCount) {
            this.totalCount = totalCount;
        }

        public List<SeverFileEntity> getFiles() {
            return files;
        }

        public void setFiles(List<SeverFileEntity> files) {
            this.files = files;
        }

        @Override
        public String toString() {
            return "FileListData{" +
                    "pageIndex=" + pageIndex +
                    ", pageSize=" + pageSize +
                    ", totalCount=" + totalCount +
                    ", files=" + files +
                    '}';
        }
    }

}


