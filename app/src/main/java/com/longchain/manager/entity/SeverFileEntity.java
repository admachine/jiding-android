package com.longchain.manager.entity;

/**
 * Created by huangyulu on 2018/6/30.
 */
public class SeverFileEntity {
//    file_id": 29,
//            "name": "06.jpg",
//            "size": 8279,
//            "time": "2018-05-23 15:57:12",
//            "url": "/api/v0/nas/file/server?file_id=29&sid=ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SmxlSEFpT2pFMU1qY3hNamt3TnpRc0ltbHpjeUk2SW1sbmFuTnJkQ0o5LjF6M0FackZtdm16a1hvLVg3RldJVThsRUoxb21LZmJQczNLbVpPbFdIWlk=",
//            "thumbnail": "/api/v0/nas/thumbnailLocal?width=200&height=200&file_id=29&sid=ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SmxlSEFpT2pFMU1qY3hNamt3TnpRc0ltbHpjeUk2SW1sbmFuTnJkQ0o5LjF6M0FackZtdm16a1hvLVg3RldJVThsRUoxb21LZmJQczNLbVpPbFdIWlk="
    private long file_id;
    private String name;
    private long size;
    String time;
    String url;
    String thumbnail;

    String file_type;


    public long getFile_id() {
        return file_id;
    }

    public void setFile_id(long file_id) {
        this.file_id = file_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getFile_type() {
        return file_type;
    }

    public void setFile_type(String file_type) {
        this.file_type = file_type;
    }
}
