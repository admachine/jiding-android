package com.longchain.manager.entity;

/**
 * Created by Administrator on 2018/4/19.
 */

public class LongChainFile {
    private String ext;// 扩展名


    private String file_type;    //文件类型string


    private String name;    //


    private String path;


    private long size;


    private String time;

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public String getFile_type() {
        return file_type;
    }

    public void setFile_type(String file_type) {
        this.file_type = file_type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "LongChainFile{" +
                "ext='" + ext + '\'' +
                ", file_type='" + file_type + '\'' +
                ", name='" + name + '\'' +
                ", path='" + path + '\'' +
                ", size=" + size +
                ", time='" + time + '\'' +
                '}';
    }

    public String getPictureType() {
        return file_type;
    }
}
