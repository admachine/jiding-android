package com.longchain.manager;

import android.util.Log;

import com.hh.base.MyUtils;
import com.hh.base.util.JsonUtils;
import com.hh.base.util.MD5Utils;
import com.longchain.common.base.LongchainApplication;
import com.tsy.sdk.myokhttp.response.JsonResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Administrator on 2018/4/8.
 */

public class DeviceManager {
    private static final String TAG = "DeviceManager";
   public static String bound ="/device/bound";
    private static DeviceManager instance;
    public static DeviceManager getInstance(){
        if (instance == null){
            synchronized (DeviceManager.class){
                if (instance == null){
                    instance  = new DeviceManager();
                }
            }
        }
        return instance;
    }
    private DeviceManager(){

    }

    public void getDiviceList(JsonResponseHandler responseHandler){
        LongchainApplication.getInstance().getMyOkHttp().get().url(bound).tag(this).addHeader("Authorization",AccountManager.token).enqueue(responseHandler);
    }

}
