package com.longchain.manager.util;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.hh.base.log.SLog;
import com.hh.base.util.NetworkUtils;
import com.hh.base.util.UIUtil;
import com.hjy.http.upload.FileUploadInfo;
import com.hjy.http.upload.FileUploadManager;
import com.hjy.http.upload.UploadOptions;
import com.hjy.http.upload.listener.OnUploadListener;
import com.hjy.http.upload.listener.OnUploadProgressListener;
import com.hjy.http.upload.progressaware.ProgressAware;
import com.luck.picture.lib.entity.LocalMedia;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2018/4/25.
 */

public class UploadUtil {
    private static final String TAG = "UploadUtil";

    private static final String UPLOAD_URL = "http://myzexabox.6655.la:8020/zbx" +
            "/file/upload";


    public static void upload(final List<LocalMedia> images, final Context context){
        if (images.size()<= 0){
            return;
        }

        if (NetworkUtils.isUseMobileType()){
            UIUtil.showConfirmDialog(context, "温馨提示", "当前处于移动网络，已将任务添加至传输列表，等待wifi开启后自动开始任务，你也可以开启使用移动网络上传下载", new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            }, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    beginUpload(context, images);
                }
            },"开启","我知道了");
        }else{
            beginUpload(context, images);
        }


    }

    private static void beginUpload(Context context, List<LocalMedia> images) {
        Toast.makeText(context,"已添加到上传列表",Toast.LENGTH_SHORT).show();
        for (LocalMedia media:images){
            if (media!=null){
                String zfsFile = "";
                int start=media.getPath().lastIndexOf("/");
                if (start != -1){
                    zfsFile = media.getPath().substring(start+1);
                }
                java.util.Map paramMap = new HashMap<>();
                paramMap.put("path","/");
                paramMap.put("zfsFile","@"+zfsFile);


                FileUploadManager.getInstance().uploadFile(null,paramMap,
                        media.getPath(), media.getPath(),/*"multipart/form-data"*/String.valueOf(media.getMimeType()),
                        UPLOAD_URL, new OnUploadListener() {
                            @Override
                            public void onError(FileUploadInfo uploadData, int errorType, String msg) {
                                SLog.e(TAG,"upload error:"+msg + errorType);
                            }

                            @Override
                            public void onSuccess(FileUploadInfo uploadData, Object data) {
                                SLog.d(TAG,"upload success:"+data);
                            }
                        },
                        new OnUploadProgressListener() {
                            @Override
                            public void onProgress(long totalSize, long currSize, int progress) {
                                SLog.d(TAG,"upload onProgress:"+progress);
                            }
                        },null);
            }
        }
    }

    public static void upload(ProgressAware progressAware, /*Map<String, String> paramMap,*//* String id,*/
                              String filePath, String mimeType, OnUploadListener apiCallback,
                              /*OnUploadProgressListener uploadProgressListener,*/ UploadOptions options){
//        if (images.size()<= 0){
//            return;
//        }
//        Toast.makeText(this,"已添加到上传列表",Toast.LENGTH_SHORT).show();
//        for (LocalMedia media:images){
            if (!TextUtils.isEmpty(filePath)){
                String zfsFile = "";
                int start=filePath.lastIndexOf("/");
                if (start != -1){
                    zfsFile = filePath.substring(start+1);
                }
                java.util.Map paramMap = new HashMap<>();
                paramMap.put("path","/");
                paramMap.put("zfsFile","@"+zfsFile);


                FileUploadManager.getInstance().uploadFile(progressAware,paramMap,
                        filePath, filePath,String.valueOf(mimeType),
                        UPLOAD_URL, apiCallback,options);
            }
//        }
    }
    public static void upload( String filePath, String mimeType, OnUploadListener apiCallback,
                              UploadOptions options){
           upload(null,filePath,mimeType,apiCallback,options);
    }
    public static void upload( String filePath, String mimeType, OnUploadListener apiCallback){
           upload(filePath,mimeType,apiCallback,null);
    }
}
