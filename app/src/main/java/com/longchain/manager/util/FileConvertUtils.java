package com.longchain.manager.util;

import com.longchain.manager.entity.LongChainFile;
import com.longchain.manager.entity.SeverFileEntity;
import com.luck.picture.lib.entity.LocalMedia;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dh on 2018/5/1.
 */

public class FileConvertUtils {

    public static LocalMedia getLocalMedia(SeverFileEntity longChainFile){
        LocalMedia localMedia = new LocalMedia(longChainFile.getUrl(),0
                ,Integer.valueOf(longChainFile.getFile_type()),"");
        return localMedia;
    }

    public static List<LocalMedia> getLocalMediaList(List<SeverFileEntity> longChainFileList ){
        List<LocalMedia> result = new ArrayList<>();
        for (SeverFileEntity longChainFile : longChainFileList){
            result.add(getLocalMedia(longChainFile));
        }
        return result;
    }
}
