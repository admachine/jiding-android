package com.longchain.common.base;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by 28367 on 2018/3/17.
 */
public class ActivityStack {

    private List<WeakReference<Activity>> mActivity = new ArrayList<>();
    private int mStartActivityNum = 0;
    private WeakReference<Activity> mTopActivity;

    public int getActivityNum() {
        return mActivity.size();
    }

    public int getStartActivityNum() {
        return mStartActivityNum;
    }

    public void finishAllActivity(Matcher matcher) {
        for (WeakReference<Activity> activity : mActivity) {
            if (isActivityValid(activity)) {
                if (matcher.isMatch(activity.get())) {
                    activity.get().finish();
                }
            }
        }
    }

    public boolean isActivityExist(Class claz) {
        for (WeakReference<Activity> activity : mActivity) {
            if (isActivityValid(activity)) {
                if (activity.get().getClass().equals(claz)) {
                    return true;
                }
            }
        }
        return false;
    }

    public int countOfActivity(Class claz) {
        int activityExist = 0;
        for (WeakReference<Activity> activity : mActivity) {
            if (isActivityValid(activity)) {
                if (activity.get().getClass().equals(claz)) {
                    activityExist++;
                }
            }
        }
        return activityExist;
    }

    public void finishAllActivity() {
        for (WeakReference<Activity> activity : mActivity) {
            if (isActivityValid(activity)) {
                activity.get().finish();
            }
        }
        mActivity.clear();
    }

    public Context getActivity(Class clazz) {
        for (WeakReference<Activity> activity : mActivity) {
            if (isActivityValid(activity)) {
                if (activity.get().getClass().equals(clazz)) {
                    return activity.get();
                }
            }
        }
        return null;
    }

    public Context getTopContext() {;
        Context context = null;
        if (isActivityValid(mTopActivity)) {
            context = mTopActivity.get();
        } else {
            if (mActivity.size() > 0) {
                context = mActivity.get(mActivity.size() - 1).get();
            }
            if (context == null) {
                    context = LongchainApplication.getContext();
            }
        }
        return context;
    }

    @Nullable
    public Activity getTopActivity() {
        return mTopActivity == null ? null : mTopActivity.get();
    }

    public Activity getActivityUnderTop() {
        if (getActivityNum() < 2) {
            return null;
        }
        return mActivity.get(mActivity.size() - 2).get();
    }

    public void add(Activity activity) {
        if (null == activity) {
            return;
        }
        for (WeakReference<Activity> ac : mActivity) {
            Activity realActivity = ac.get();
            if (null != realActivity && realActivity == activity) {
                mActivity.remove(ac);
                break;
            }
        }
        mActivity.add(new WeakReference<Activity>(activity));
    }

    public void remove(Activity activity) {
        if (null == activity) {
            return;
        }
        Iterator<WeakReference<Activity>> it = mActivity.iterator();
        while (it.hasNext()) {
            WeakReference<Activity> ac = it.next();
            Activity realActivity = ac.get();
            if ((null == realActivity) || (realActivity == activity)) {
                it.remove();
            }
        }
    }

    public void onStart() {
        mStartActivityNum++;
    }

    public void onStop() {
        if (mStartActivityNum > 0) {
            mStartActivityNum--;
        }
    }

    public void onResume(Activity activity) {
        mTopActivity = new WeakReference<>(activity);
    }

    public void onPause(Activity activity) {
        if (isActivityValid(mTopActivity)) {
            if (mTopActivity.get() == activity) {
                mTopActivity.clear();
                mTopActivity = null;
            }
        }
    }

    public abstract static class Matcher{
        public abstract boolean isMatch(Activity activity);
    }

    public void clearTop(Matcher matcher, Activity thiz) {
        if (null == matcher) {
            return;
        }
        boolean find = false;
        for (int i = 0; i < mActivity.size(); ++i) {
            Activity activity = mActivity.get(i).get();
            if (null == activity) {
                continue;
            }
            if (activity == thiz) {
                find = true;
            }
            if (find && matcher.isMatch(activity)) {
                activity.finish();
                continue;
            }
        }
    }

    private boolean isActivityValid(WeakReference<Activity> activityWeakReference) {
        return activityWeakReference != null
                && activityWeakReference.get() != null && !activityWeakReference.get().isFinishing();
    }


}

