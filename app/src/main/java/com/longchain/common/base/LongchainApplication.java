package com.longchain.common.base;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;
import com.hh.base.MyUtils;
import com.hjy.http.download.DownloadConfiguration;
import com.hjy.http.download.DownloadManager;
import com.hjy.http.upload.FileUploadConfiguration;
import com.hjy.http.upload.FileUploadManager;
import com.tsy.sdk.myokhttp.MyOkHttp;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by 28367 on 2018/3/17.
 */

public class LongchainApplication extends MultiDexApplication {

    private final String TAG = getClass().getSimpleName();
    public static Context gContext;
    private static LongchainApplication mInstance;
    private static MyOkHttp mMyOkHttp;
    public static ActivityStack gStack = new ActivityStack();
    public static Context getContext() {
        return gContext;
    }

    public static String readString(int resId) {
        return gContext.getString(resId);
    }

    public static float readDimension(int resId) {
        return gContext.getResources().getDimension(resId);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        gContext = this;
        mInstance = this;
        MyUtils.init(gContext);
        initOkHttp();
        initUploadAndDownload();
        registerActivityLifecycleLog();
    }

    private void initUploadAndDownload() {
        FileUploadConfiguration fileUploadConfiguration = new  FileUploadConfiguration.Builder(this)
//                .setResponseProcessor(...)  //设置http response字符串的结果解析器，如果不设置，则默认返回response字符串
              .setThreadPoolSize(5)		 //设置线程池大小，如果采用默认的线程池则有效
                .setThreadPriority(Thread.NORM_PRIORITY - 1)  //设置线程优先级，如果采用默认的线程池则有效
//                .setTaskExecutor(...)     //设置自定义的线程池
//              .setFileUploader(...)     //设置自定义的文件上传功能，如果不设置则采用默认的文件上传功能
              .build();
        FileUploadManager.getInstance().init(fileUploadConfiguration);

        DownloadConfiguration downloadConfiguration = new DownloadConfiguration.Builder(getApplicationContext())
//                .setCacheDir(...)        //设置下载缓存目录，必须设置
//              .setTaskExecutor(...)    //同上传类似
//              .setThreadPriority(...)  //同上传类似
//              .setThreadPoolCoreSize(...)  //同上传类似
  			.build();
        DownloadManager.getInstance(this).init(downloadConfiguration);
    }

    private void initOkHttp(){
        //持久化存储cookie
        ClearableCookieJar cookieJar =
                new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(getApplicationContext()));

        //log拦截器
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        //自定义OkHttp
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(10000L, TimeUnit.MILLISECONDS)
                .readTimeout(10000L, TimeUnit.MILLISECONDS)
                .cookieJar(cookieJar)       //设置开启cookie
                .addInterceptor(logging)            //设置开启log
                .build();
        mMyOkHttp = new MyOkHttp(okHttpClient);
    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
    private void registerActivityLifecycleLog() {
        registerActivityLifecycleCallbacks(new Application.ActivityLifecycleCallbacks() {

            @Override
            public void onActivityCreated(Activity activity, Bundle bundle) {
                gStack.add(activity);
            }

            @Override
            public void onActivityStarted(Activity activity) {
                gStack.onStart();
            }

            @Override
            public void onActivityResumed(Activity activity) {
                gStack.onResume(activity);
            }

            @Override
            public void onActivityPaused(Activity activity) {
                gStack.onPause(activity);
            }

            @Override
            public void onActivityStopped(Activity activity) {
                gStack.onStop();
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {
                gStack.remove(activity);
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public void registerActivityLifecycleCallback(Application.ActivityLifecycleCallbacks callbacks) {
        registerActivityLifecycleCallbacks(callbacks);
    }

    public static synchronized LongchainApplication getInstance() {
        return mInstance;
    }

    public MyOkHttp getMyOkHttp() {
        return mMyOkHttp;
    }
}
