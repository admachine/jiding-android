package com.longchain.common.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.hh.base.dialog.SubmitMessageDialog;
import com.hh.base.log.SLog;
import com.hh.base.util.UIUtil;
import com.hjy.http.download.DownloadManager;
import com.hjy.http.download.FileDownloadTask;
import com.hjy.http.download.listener.OnDownloadingListener;
import com.longchain.R;
import com.longchain.business.search.OnGetSelectImages;
import com.longchain.manager.entity.LongChainFile;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 */

public class BottomFileOperationLayout extends LinearLayout {

    private static final String TAG = "BottomFileOperationLayout";

    public BottomFileOperationLayout(@NonNull Context context) {
        super(context);
        init(null);
    }

    public BottomFileOperationLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public BottomFileOperationLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        LayoutInflater.from(getContext()).inflate(R.layout.bottom_layout, this);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.download_layout, R.id.delete_layout, R.id.edit_layout})
    public void onViewClicked(View view) {

        switch (view.getId()) {
            case R.id.download_layout:
                if (onOperationListener!=null){
                    onOperationListener.onDownLoadClick();
                }

                break;

            case R.id.delete_layout:
                if (onOperationListener!=null){
                    onOperationListener.onDeleteClick();
                }


                break;
            case R.id.edit_layout:
                if (onOperationListener!=null){
                    onOperationListener.onEditTagClick();
                }
                UIUtil.showSubmitDialog(getContext(), "添加tag", "", 6, new SubmitMessageDialog.OnComfirmClickListener() {
                    @Override
                    public void onConfirmClick(View view, String content) {

                    }
                });
                break;

        }
    }

    private OnOperation onOperationListener;

    public void setOnOperationListener(OnOperation onOperationListener) {
        this.onOperationListener = onOperationListener;
    }

    public interface OnOperation {
        void onDeleteClick();

        void onDownLoadClick();

        void onEditTagClick();
    }
}
