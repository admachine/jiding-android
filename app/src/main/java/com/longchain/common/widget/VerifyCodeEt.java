package com.longchain.common.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.longchain.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by 28367 on 2018/3/18.
 */

public class VerifyCodeEt extends FrameLayout implements TextWatcher, View.OnKeyListener{
    @BindView(R.id.verify_code1)
    EditText mVerifyCode1;
    @BindView(R.id.verify_code2)
    EditText mVerifyCode2;
    @BindView(R.id.verify_code3)
    EditText mVerifyCode3;
    @BindView(R.id.verify_code4)
    EditText mVerifyCode4;
    private List<EditText> mEditTextList = new ArrayList<>();
    private int currentPosition = 0;
    private Listener listener;

    public VerifyCodeEt(@NonNull Context context) {
        super(context);
        init(null);
    }

    public VerifyCodeEt(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public VerifyCodeEt(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        LayoutInflater.from(getContext()).inflate(R.layout.verifycode_layout, this);
        ButterKnife.bind(this);
        mEditTextList.add(mVerifyCode1);
        mEditTextList.add(mVerifyCode2);
        mEditTextList.add(mVerifyCode3);
        mEditTextList.add(mVerifyCode4);
        mVerifyCode1.addTextChangedListener(this);
        mVerifyCode2.addTextChangedListener(this);
        mVerifyCode3.addTextChangedListener(this);
        mVerifyCode4.addTextChangedListener(this);
        mVerifyCode1.setOnKeyListener(this);
        mVerifyCode2.setOnKeyListener(this);
        mVerifyCode3.setOnKeyListener(this);
        mVerifyCode4.setOnKeyListener(this);
    }

    private void backFocus() {
        EditText editText ;
        for (int i = mEditTextList.size()-1; i>= 0; i--) {
            editText = mEditTextList.get(i);
            if (editText.getText().length() == 1) {
                editText.requestFocus();
                editText.setSelection(1);
                return;
            }
        }
    }

    private void focus() {
        EditText editText ;
        for (int i = 0; i< mEditTextList.size(); i++) {
            editText = mEditTextList.get(i);
            if (editText.getText().length() < 1) {
                editText.requestFocus();
                return;
            }
        }
    }

    private void checkAndCommit() {
        StringBuilder stringBuilder = new StringBuilder();
        boolean full = true;
        for (int i = 0 ;i < mEditTextList.size(); i++){
            EditText editText = mEditTextList.get(i);
            String content = editText.getText().toString();
            if ( content.length() == 0) {
                full = false;
                break;
            } else {
                stringBuilder.append(content);
            }
        }
        if (full){
            if (listener != null) {
                listener.onComplete(stringBuilder.toString());
                setEnabled(false);
            }
        }
    }

    @Override
    public void setEnabled(boolean enabled) {

        for (int i = 0; i < mEditTextList.size(); i++) {
            mEditTextList.get(i).setEnabled(enabled);
        }
    }

    public void setOnCompleteListener(Listener listener){
        this.listener = listener;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (start == 0 && count >= 1 && currentPosition != mEditTextList.size() - 1) {
            currentPosition++;
            mEditTextList.get(currentPosition).requestFocus();
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (s.length() == 0) {
        } else {
            focus();
            checkAndCommit();
        }
    }

    @Override
    public boolean onKey(View view, int keyCode, KeyEvent event) {
        EditText editText = (EditText) view;
        if (keyCode == KeyEvent.KEYCODE_DEL && editText.getText().length() == 0) {
            int action = event.getAction();
            if (currentPosition != 0 && action == KeyEvent.ACTION_DOWN) {
                currentPosition--;
                mEditTextList.get(currentPosition).requestFocus();
                mEditTextList.get(currentPosition).setText("");
            }
        }
        return false;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return super.onKeyDown(keyCode, event);
    }

    public interface Listener {
        void onComplete(String content);
    }

}
