package com.hh.base;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Administrator on 2018/4/11.
 */

public class MyUtils {
  public static final String HOST ="http://222.188.110.100:8020/api/v0";
    public static Context mContext;
    public static String token;


    public static void init(Context context){
        mContext = context;
    }

    public static String getToken(){
        if (token == null){
            Toast.makeText(mContext,"请先登录",Toast.LENGTH_LONG).show();
        }
        return token;
    }

}
