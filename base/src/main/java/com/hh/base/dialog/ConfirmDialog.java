package com.hh.base.dialog;

import android.app.Dialog;
import android.content.Context;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import com.hh.base.R;


/**
 * Created by Dell on 2015/7/24 0024.
 */
public class ConfirmDialog extends Dialog{
    private TextView mDialogContentTV4;
    private TextView mDialogOkTV4;
    private TextView mDialogCancelTV4;
    private TextView mTitleTV4;
    private View.OnClickListener mOnConfirmListener;
    private View.OnClickListener mOnCancelListener;
    private boolean isCanceledOnTouchOutside;

    public ConfirmDialog(Context context, String title, SpannableString content) {
        super(context, R.style.DialogFullStyle);
        init(context,title,content);
    }

    public ConfirmDialog(Context context, String title, String content) {
        super(context,R.style.DialogFullStyle);
        init(context, title, new SpannableString(content));
    }

    private void init(Context context, String title, SpannableString content){
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_confirm);
        setCanceledOnTouchOutside(false);
        mDialogOkTV4 = (TextView) findViewById(R.id.confirm);
        mDialogCancelTV4 = (TextView) findViewById(R.id.cancel);
        mDialogCancelTV4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if(mOnCancelListener!=null){
                    mOnCancelListener.onClick(v);
                }
            }
        });
        mDialogOkTV4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if(mOnConfirmListener!=null){
                    mOnConfirmListener.onClick(v);
                }
            }
        });
        mTitleTV4 = (TextView) findViewById(R.id.dialog_title_tv);
        mDialogContentTV4 = (TextView) findViewById(R.id.dialog_content_tv);

        if(!TextUtils.isEmpty(title)) {
            mTitleTV4.setText(title);
        }
        if(!TextUtils.isEmpty(content)) {
            mDialogContentTV4.setText(content);
        }
    }

    public void setOnConfirmListener(View.OnClickListener onConfirmListener){
//        mDialogOkTV4.setOnClickListener(onConfirmListener);
        mOnConfirmListener = onConfirmListener;
    }

    public void setOnCancelListener(View.OnClickListener onCancelListener){
//        mDialogOkTV4.setOnClickListener(onConfirmListener);
        mOnCancelListener = onCancelListener;
    }

    public void setCancelBtnVisiable(boolean cancelAble){
        if(cancelAble){
            mDialogCancelTV4.setVisibility(View.VISIBLE);
        }else{
            mDialogCancelTV4.setVisibility(View.GONE);
        }
    }

    public void setConfirmBtnVisiable(boolean cancelAble){
        if(cancelAble){
            mDialogOkTV4.setVisibility(View.VISIBLE);
        }else{
            mDialogOkTV4.setVisibility(View.GONE);
        }
    }

    public void switchBtnColor(){
//        mDialogCancelTV4.setTextColor(getContext().getResources().getColor(R.color.new_c1));
//        mDialogOkTV4.setTextColor(getContext().getResources().getColor(R.color.new_c4));
    }

    public void setBtnText(String cancelText, String confirmText) {
        if(!TextUtils.isEmpty(cancelText)) {
            mDialogCancelTV4.setText(cancelText);
        }
        if(!TextUtils.isEmpty(confirmText)) {
            mDialogOkTV4.setText(confirmText);
        }
    }

    @Override
    public void setCanceledOnTouchOutside(boolean cancel) {
        isCanceledOnTouchOutside = cancel;
        super.setCanceledOnTouchOutside(cancel);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(isCanceledOnTouchOutside) {
            return super.onKeyDown(keyCode, event);
        }else{
            return false;
        }
    }
}
