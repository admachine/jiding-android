package com.hh.base.dialog;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by Dell on 2016/1/14 0014.
 */
public class TextWatcherImpl implements TextWatcher {

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
