package com.hh.base.dialog;

import android.app.Dialog;
import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.hh.base.R;


/**
 * Created by Dell on 2015/7/24 0024.
 */
public class SubmitMessageDialog extends Dialog {

    private EditText etSubmitMessage;
    private TextView tvLimitCount;
    private TextView tvComfire;
    private TextView tvCancle;
    private TextView tvTitle;
    private OnComfirmClickListener onComfirmClickListener;
    private View.OnClickListener mOnCancelListener;

    private boolean isCanceledOnTouchOutside;

    private TextChangeInterface textChangeInterface;

    private Context context;
    private int inputType =InputType.TYPE_CLASS_TEXT;

    public void setTextChangeInterface(TextChangeInterface textChangeInterface) {
        this.textChangeInterface = textChangeInterface;
    }

    public interface OnComfirmClickListener {
        void onConfirmClick(View view, String content);
    }

    public SubmitMessageDialog(Context context, String title, String editTextHint, int contentLengthLimit) {
        super(context, R.style.DialogFullStyle);
        this.context = context;
        init(title, editTextHint, contentLengthLimit);
    }
    public SubmitMessageDialog(Context context, String title, String editTextHint, int contentLengthLimit, int inputType) {
        super(context, R.style.DialogFullStyle);
        this.context = context;
        this.inputType = inputType;
        init(title, editTextHint, contentLengthLimit);
    }

    private void init(String title, final String editTextHint, final int contentLengthLimit) {
        setContentView(R.layout.dialog_submit_message);
//        setCanceledOnTouchOutside(false);

        tvTitle = (TextView) findViewById(R.id.dialog_title_tv);
        tvTitle.setText(title);

        tvComfire = (TextView) findViewById(R.id.confirm);
        tvComfire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (onComfirmClickListener != null) {
                    onComfirmClickListener.onConfirmClick(v, etSubmitMessage.getText().toString());
                }
            }
        });

        tvCancle = (TextView) findViewById(R.id.cancel);
        tvCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (mOnCancelListener != null) {
                    mOnCancelListener.onClick(v);
                }
            }
        });

        etSubmitMessage = (EditText) findViewById(R.id.et_submit_message);
        etSubmitMessage.setHint(editTextHint);
        InputFilter[] filters = {new InputFilter.LengthFilter(contentLengthLimit)};
        etSubmitMessage.setFilters(filters);
        etSubmitMessage.setInputType(inputType);
        etSubmitMessage.addTextChangedListener(new TextWatcherImpl() {
            @Override
            public void afterTextChanged(Editable s) {
                super.afterTextChanged(s);
                int currentLimit = contentLengthLimit - s.length();
                tvLimitCount.setText(currentLimit + "");
                if (textChangeInterface != null) {
                    textChangeInterface.textChange(s);
                }
            }
        });

        tvLimitCount = (TextView) findViewById(R.id.tv_limit_count);
        tvLimitCount.setText(contentLengthLimit + "");

        getWindow().getDecorView().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (context != null) {
                    InputMethodManager mInputManger = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (mInputManger != null) {
                        if (etSubmitMessage != null) {
                            etSubmitMessage.requestFocus();
                            mInputManger.showSoftInput(etSubmitMessage, 0);

                        }
                    }
                }
            }
        },100);
    }

    public interface TextChangeInterface {
        void textChange(Editable s);
    }

    public void setOnComfirmClickListener(OnComfirmClickListener onComfirmClickListener) {
        this.onComfirmClickListener = onComfirmClickListener;
    }

    public void setOnCancelListener(View.OnClickListener onCancelListener) {
        mOnCancelListener = onCancelListener;
    }

    public void setConfireButtonClickable(boolean clickable) {
        tvComfire.setClickable(clickable);
        if(clickable){
            tvComfire.setTextColor(0xFFff9d00);
        }else{
            tvComfire.setTextColor(0xcccccc);
        }
    }

    public EditText getMessageEditText(){
        return etSubmitMessage;
    }

    public String getMessage() {
        return etSubmitMessage.getText().toString();
    }

    @Override
    public void setCanceledOnTouchOutside(boolean cancel) {
        isCanceledOnTouchOutside = cancel;
        super.setCanceledOnTouchOutside(cancel);
    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (isCanceledOnTouchOutside) {
//            return super.onKeyDown(keyCode, event);
//        } else {
//            return false;
//        }
//    }
}
