package com.hh.base.util;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Looper;
import android.text.SpannableString;
import android.view.View;
import android.widget.Toast;

import com.hh.base.MyUtils;
import com.hh.base.dialog.ConfirmDialog;
import com.hh.base.dialog.PictureDialog;
import com.hh.base.dialog.SubmitMessageDialog;


/**
 * Created by Dell on 2015/4/13 0013.
 */
public class UIUtil {
    public static void showToast(final String message) {
        Context context = MyUtils.mContext;
        if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
           Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
        } else {

        }
    }
    public static void showToast(String message,String errMsg,int errorCode) {
        Context context = MyUtils.mContext;
        if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
           Toast.makeText(context,String.format("%s: %s code =%d",message,errMsg,errorCode),Toast.LENGTH_SHORT).show();
        } else {

        }
    }


    public static void loadingCancel() {
        //进度框对话条,用try catch 来处理
        try {
            if (mProgressDialog != null) {
                if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
            }
        } catch (final IllegalArgumentException e) {
            // Handle or log or ignore
        } catch (final Exception e) {
            // Handle or log or ignore
        } finally {
            mProgressDialog = null;
        }
    }
    private static PictureDialog mProgressDialog;
    public static void loadingShow(Context ctx) {
        loadingCancel();
        mProgressDialog = new PictureDialog(ctx);
//        mProgressDialog.setCanceledOnTouchOutside(false);
//        mProgressDialog.setOnDismissListener(dismissListener);
        mProgressDialog.show();
    }

   /* private static LoadingDialog mProgressDialog;
    private static CocoToast sSingleToast;
    private static long sLastShowTime = 0;





    public static void showNoRepeatToast(final String message){
        showNoRepeatToast(message,5000);
    }

    public static void showNoRepeatToast(final String message,final long delay){
        if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
            showNoRepeatToastInternal(message,delay);
        } else {
            CommonMainHandler.getInstance().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showNoRepeatToastInternal(message,delay);
                }
            });
        }
    }

    private static void showNoRepeatToastInternal(String message,long delay){
        if(sSingleToast == null){
            sSingleToast = new CocoToast(CommonApplication.getContext());
        }
        if(message.equals(sSingleToast.getMessage())){
            if(System.currentTimeMillis() - sLastShowTime >= delay){
                sLastShowTime = System.currentTimeMillis();
                sSingleToast.show(message);
            }
        }else{
            if(!TextUtils.isEmpty(sSingleToast.getMessage())){
                sSingleToast.cancel();
            }
            sLastShowTime = System.currentTimeMillis();
            sSingleToast.show(message);
        }
    }

    *//**
     * Loging弹框
     *//*
    public static void progressShow(Context ctx) {
        progressShow("", ctx, null);
    }

    *//**
     * Loging弹框
     *
     * @param msg
     *//*
    public static void progressShow(String msg, Context ctx) {
        progressShow(msg, ctx, null);
    }

    *//**
     * Loging弹框
     *
     * @param msg
     * @param dismissListener
     *//*
    public static void progressShow(String msg, Context ctx, DialogInterface.OnDismissListener dismissListener) {
        Context context = PluginUtils.checkPluginContext(ctx);
        loadingCancel();
        mProgressDialog = new LoadingDialog(context, msg);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setOnDismissListener(dismissListener);
        mProgressDialog.show();
    }



    *//**
     * 取消等待弹框
     *//*
    public static void loadingCancel() {
        //进度框对话条,用try catch 来处理
        try {
            if (mProgressDialog != null) {
                if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
            }
        } catch (final IllegalArgumentException e) {
            // Handle or log or ignore
        } catch (final Exception e) {
            // Handle or log or ignore
        } finally {
            mProgressDialog = null;
        }
    }
*/
    public static ConfirmDialog showAlertDialog(Context context, String content) {
        ConfirmDialog confirmDialog = new ConfirmDialog(context, "", content);
        confirmDialog.setCancelBtnVisiable(false);
        confirmDialog.setCanceledOnTouchOutside(true);
        confirmDialog.show();
        return confirmDialog;
    }

    public static ConfirmDialog showAlertDialog(Context context, String title, SpannableString content) {
        ConfirmDialog confirmDialog = new ConfirmDialog(context, title, content);
        confirmDialog.setCancelBtnVisiable(false);
        confirmDialog.setCanceledOnTouchOutside(true);
        confirmDialog.show();
        return confirmDialog;
    }

    public static ConfirmDialog showAlertDialog(Context context, String title, String content) {
        ConfirmDialog confirmDialog = new ConfirmDialog(context, title, content);
        confirmDialog.setCancelBtnVisiable(false);
        confirmDialog.setCanceledOnTouchOutside(true);
        confirmDialog.show();
        return confirmDialog;
    }

    public static ConfirmDialog showAlertDialog(Context context, String title, String content, boolean cancelAble) {
        ConfirmDialog confirmDialog = new ConfirmDialog(context, title, content);
        confirmDialog.setCancelBtnVisiable(false);
        confirmDialog.setConfirmBtnVisiable(cancelAble);
        confirmDialog.setCanceledOnTouchOutside(true);
        confirmDialog.show();
        return confirmDialog;
    }

    public static void showAlertDialog(Context context, String title, String content, final View.OnClickListener onConfirmListener) {
        ConfirmDialog confirmDialog = new ConfirmDialog(context, title, content);
        confirmDialog.setCancelBtnVisiable(false);
        confirmDialog.setOnConfirmListener(onConfirmListener);
        confirmDialog.setCanceledOnTouchOutside(true);
        confirmDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (onConfirmListener != null) {
                    onConfirmListener.onClick(null);
                }
            }
        });
        confirmDialog.show();
    }

    public static void showAlertDialog(Context context, String title, String content, final View.OnClickListener onConfirmListener,String confirmText) {
        ConfirmDialog confirmDialog = new ConfirmDialog(context, title, content);
        confirmDialog.setCancelBtnVisiable(false);
        confirmDialog.setOnConfirmListener(onConfirmListener);
        confirmDialog.setCanceledOnTouchOutside(true);
        confirmDialog.setBtnText("", confirmText);
        confirmDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (onConfirmListener != null) {
                    onConfirmListener.onClick(null);
                }
            }
        });
        confirmDialog.show();
    }

//    *//*
//    黄色取消，灰色确认
//     *//*
    public static ConfirmDialog showCancelDialog(Context context, String title, String content, View.OnClickListener onConfirmListener) {
        ConfirmDialog confirmDialog = new ConfirmDialog(context, title, content);
        confirmDialog.setOnConfirmListener(onConfirmListener);
        confirmDialog.show();
        return confirmDialog;
    }

//    *//*
//    黄色确认，灰色取消
//     *//*
    public static ConfirmDialog showConfirmDialog(Context context, String title, String content, View.OnClickListener onConfirmListener) {
        ConfirmDialog confirmDialog = new ConfirmDialog(context, title, content);
        confirmDialog.setOnConfirmListener(onConfirmListener);
        confirmDialog.show();
        return confirmDialog;
    }

//    *//*
//   黄色确认，灰色取消
//    *//*
    public static ConfirmDialog showConfirmDialog(Context context, String title, String content, View.OnClickListener onConfirmListener,String confirmText) {
        ConfirmDialog confirmDialog = new ConfirmDialog(context, title, content);
        confirmDialog.setCancelBtnVisiable(false);
        confirmDialog.setBtnText("", confirmText);
        confirmDialog.setCanceledOnTouchOutside(true);
        confirmDialog.setOnConfirmListener(onConfirmListener);
        confirmDialog.show();
        return confirmDialog;
    }


//    *//*
//    黄色确认，灰色取消
//     *//*
    public static ConfirmDialog showConfirmDialog(Context context, String title, String content, View.OnClickListener onConfirmListener, String cancelText, String confirmText) {
        ConfirmDialog confirmDialog = new ConfirmDialog(context, title, content);
        confirmDialog.setOnConfirmListener(onConfirmListener);
        confirmDialog.setBtnText(cancelText, confirmText);
        confirmDialog.show();
        return confirmDialog;
    }

//    *//*
//  黄色确认，灰色取消
//   *//*
    public static ConfirmDialog showConfirmDialog(Context context, String title, String content, View.OnClickListener onConfirmListener, View.OnClickListener onCancelListener, String cancelText, String confirmText) {
        ConfirmDialog confirmDialog = new ConfirmDialog(context, title, content);
        confirmDialog.setOnConfirmListener(onConfirmListener);
        confirmDialog.setOnCancelListener(onCancelListener);
        confirmDialog.setBtnText(cancelText, confirmText);
        confirmDialog.show();
        return confirmDialog;
    }

//    *//*
//    黄色确认，灰色取消
//     *//*
    public static ConfirmDialog showConfirmDialog(Context context, String title, SpannableString content, View.OnClickListener onConfirmListener) {
        ConfirmDialog confirmDialog = new ConfirmDialog(context, title, content);
        confirmDialog.setOnConfirmListener(onConfirmListener);
        confirmDialog.show();
        return confirmDialog;
    }
    /*
    黄色确认，灰色取消(可输入内容)
    */
    public static SubmitMessageDialog showSubmitDialog(Context context, String title, String editTextHint, int contentLengthLimit, SubmitMessageDialog.OnComfirmClickListener onComfirmClickListener) {
        SubmitMessageDialog submitMessageDialog = new SubmitMessageDialog(context, title, editTextHint, contentLengthLimit);
        submitMessageDialog.setOnComfirmClickListener(onComfirmClickListener);
        submitMessageDialog.show();
        return submitMessageDialog;
    }

//    *//*
//    黄色确认，灰色取消(可输入内容)
//    *//*
    public static SubmitMessageDialog showSubmitDialog(Context context, String title, String editTextHint, int contentLengthLimit, SubmitMessageDialog.OnComfirmClickListener onComfirmClickListener, int inputType) {
        SubmitMessageDialog submitMessageDialog = new SubmitMessageDialog(context, title, editTextHint, contentLengthLimit,inputType);
        submitMessageDialog.setOnComfirmClickListener(onComfirmClickListener);
        submitMessageDialog.show();
        return submitMessageDialog;
    }



}
