package com.hh.base.log;

/**
 * Created by Haibiao.Li on 2017/4/20 0020
 * <br>Email:lihaibiaowork@gmail.com</br>
 * <p><b>注释：</b></p>
 */

public interface ILog {
    public static final int LEVEL_V = 0;
    public static final int LEVEL_D = 1;
    public static final int LEVEL_I = 2;
    public static final int LEVEL_W = 3;
    public static final int LEVEL_E = 4;

    void print(int level, String tag, String msg);
    void flush();
}
