package com.hh.base.log;

import android.os.Looper;
import android.os.Process;
import android.text.TextUtils;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

/**
 * Created by Haibiao.Li on 2017/4/20 0020
 * <br>Email:lihaibiaowork@gmail.com</br>
 * <p><b>注释：</b></p>
 */

public class DefaultLogImpl implements ILog{
    private static final String TAG = "DefaultLogImpl";
    //"TIME [PID:ThreadName][TAG][LEVEL]MSG"
    private static final String FORMAT = "%s[%s][%s:%s][%s]%s\n";
    private final SimpleDateFormat mRowDateFormat;
    private final LogWriter mLogWriter;
    private static final String TIME_FORMAT_SSS = "yyyy-MM-dd HH:mm:ss.SSS";

    public DefaultLogImpl(String logPathName) {
        this(logPathName,null);
    }

    public DefaultLogImpl(String logPathName,String headContent) {
        mRowDateFormat = new SimpleDateFormat(TIME_FORMAT_SSS, Locale.getDefault());
        mLogWriter = new LogWriter(logPathName,headContent);
        mLogWriter.start();
    }

    private String structureRow(int level, String tag, String msg) {
        return String.format(FORMAT,
                mRowDateFormat.format(new Date()),
                levelStr(level),
                Process.myPid(),
                threadStr(),
                tag,
                msg);
    }

    @Override
    public void print(int level, String tag, String msg) {
        switch (level){
            case ILog.LEVEL_V: Log.v(tag,msg); break;
            case ILog.LEVEL_D: Log.d(tag,msg); break;
            case ILog.LEVEL_I: Log.i(tag,msg); break;
            case ILog.LEVEL_W: Log.w(tag,msg); break;
            case ILog.LEVEL_E: Log.e(tag,msg); break;
            default: Log.v(tag,msg); break;
        }
        if(level >= ILog.LEVEL_I && !mLogWriter.isQuit()){
            String outStr = structureRow(level,tag,msg);
            if(TextUtils.isEmpty(outStr)){
                return;
            }
            boolean success = mLogWriter.offerLine(outStr);
            if(!success) Log.e(TAG,"print(): mLogQueue.offer result failed !");
        }
    }

    @Override
    public void flush() {
        mLogWriter.markFlush();
    }

    private static String threadStr(){
        return Looper.myLooper() == Looper.getMainLooper() ? "Main" : Thread.currentThread().getName();
    }

    private String levelStr(int level){
        switch (level){
            case ILog.LEVEL_V: return "V";
            case ILog.LEVEL_D: return "D";
            case ILog.LEVEL_I: return "I";
            case ILog.LEVEL_W: return "W";
            case ILog.LEVEL_E: return "E";
        }
        return "Unknown";
    }

    public static class LogWriter extends Thread {
        private static final String TAG = "LogWriter";
        private static final int BUFFER_SIZE = 2 * 1024;
        private static final long INTERVAL_WRITE = 10 * 1000;
        private static final long LAST_WRITE_INTERVAL = 10 * 1000;
        private final BlockingQueue<String> mLogQueue;
        private final String mLogPathName;
        private String mHeadContent;
        private final ByteBuffer mBuffer;
        private long mTotalWriteLength = 0;
        private volatile boolean isQuit = false;
        private long mLastWriteTime = 0;

        public LogWriter(String pathName,String headContent) {
            super(TAG);
            mLogPathName = pathName;
            mHeadContent = headContent;
            mLogQueue = new LinkedBlockingDeque<>();
            mBuffer = ByteBuffer.allocate(BUFFER_SIZE);
        }

        public boolean isQuit() {
            return isQuit;
        }

        public boolean offerLine(String log){
            return mLogQueue.offer(log);
        }

        public void markFlush(){
            mLastWriteTime = 0;
        }

        private int write(FileChannel channel) throws IOException {
            mBuffer.flip();
            int length = mBuffer.limit();
            if(length > 0){
                while (channel.write(mBuffer) > 0){
                    mTotalWriteLength += length;
                }
                mLastWriteTime = System.currentTimeMillis();
            }
            mBuffer.clear();
            return length;
        }

        @Override
        public void run() {
            super.run();
            final boolean DEBUG = ((SLog.flag & SLog.MARK_DEBUG) != 0);
            try {
                File file = createLogFileIfNeed();
                FileOutputStream fos = new FileOutputStream(file,true);
                FileChannel fileChannel = fos.getChannel();
                mBuffer.clear();
                String headContent = "Log Dump Time : " + com.hh.base.util.TimeUtils.millis2String(System.currentTimeMillis(),new SimpleDateFormat(TIME_FORMAT_SSS, Locale.getDefault())) + "\n";
                headContent += com.hh.base.util.PhoneUtils.dumpPhoneInfo();
                if(!TextUtils.isEmpty(mHeadContent)){
                    headContent += mHeadContent;
                    mHeadContent = null;
                }
                writePrepare(fileChannel,headContent,true,DEBUG);
                try{
                    while (!isQuit) {
                        writePrepare(fileChannel,mLogQueue.poll(INTERVAL_WRITE, TimeUnit.MILLISECONDS),false,DEBUG);
                    }
                }catch (InterruptedException e){
                    e.printStackTrace();
                    Log.e(TAG,"run() InterruptedException : "+e.getMessage());
                    isQuit = true;
                }finally {
                    fileChannel.force(false);
                    fos.close();
                    fileChannel.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
                Log.e(TAG,"run() createLogFile IOException : "+e.getMessage());
            }finally {
                isQuit = true;
                mLogQueue.clear();
                mBuffer.clear();
            }
        }

        private void writePrepare(FileChannel fileChannel,String log,final boolean now,boolean DEBUG) throws IOException {
            if(log == null){//time out
                int length = write(fileChannel);
                if(DEBUG) Log.d(TAG,"mLogQueue.poll time out execute write length = "+length + ",mTotalWriteLength = "+mTotalWriteLength);
            }else{
                byte[] buff = log.getBytes();
                if(DEBUG) Log.d(TAG, String.format("mBuffer size = %s,log buff length = %s,remaining size = %s",
                        mBuffer.capacity(),buff.length,mBuffer.remaining()));
                if(buff.length < mBuffer.remaining()){
                    mBuffer.put(buff);
                    if(now || System.currentTimeMillis() - mLastWriteTime > LAST_WRITE_INTERVAL){
                        int length = write(fileChannel);
                        if(DEBUG) Log.d(TAG,"last write time out execute write,write length = "+length+",mTotalWriteLength = "+mTotalWriteLength);
                    }
                }else{
                    if(DEBUG) Log.d(TAG,"mBuffer capacity full execute loop write,log length = "+buff.length+",mTotalWriteLength = "+mTotalWriteLength);
                    int offset = 0;
                    do{
                        int length = Math.min(buff.length - offset,mBuffer.remaining());
                        mBuffer.put(buff,offset,length);
                        if(mBuffer.remaining() == 0 || now){
                            write(fileChannel);
                        }
                        offset += length;
                    }while (offset < buff.length);
                }
            }
        }

        private File createLogFileIfNeed() throws IOException {
            File file = new File(mLogPathName);
            boolean result = com.hh.base.util.FileUtils.createOrExistsFile(file);
            String msg = TAG + " createLogFileIfNeed result = " + result + ",path = " + mLogPathName;
            if(result){
                Log.i(TAG,msg);
                return file;
            }else{
                Log.e(TAG,msg);
                throw new IOException(msg);
            }
        }
    }
}
