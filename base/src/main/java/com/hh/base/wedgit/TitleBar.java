package com.hh.base.wedgit;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.hh.base.R;


/**
 * Created by 28367 on 2018/3/17.
 */

public class TitleBar extends FrameLayout {

    private TextView mTitleTxt;
    private ImageView mBackImg;
    private ImageView mRightIcon;
    private TextView mRightBtn;
    private TextView mRootView;


    public TitleBar(@NonNull Context context) {
        super(context);
    }

    public TitleBar(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public TitleBar(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        LayoutInflater.from(getContext()).inflate(R.layout.title_bar_layout, this);
        mTitleTxt = (TextView) findViewById(R.id.img_title);
        mBackImg = (ImageView) findViewById(R.id.img_back);
        mRightIcon = (ImageView) findViewById(R.id.img_title_question);
        mRightBtn = (TextView) findViewById(R.id.right_btn);
//        mRootView = findViewById(R.id.root_view);
        TypedArray array = getContext().obtainStyledAttributes(attrs, R.styleable.TitleBar);
        String rightBtn = array.getString(R.styleable.TitleBar_right_btn);
        String title = array.getString(R.styleable.TitleBar_title_text);
        boolean showBg = array.getBoolean(R.styleable.TitleBar_show_bg,true);
        if (!showBg){
            setBackgroundResource(0);
        }else {
            setBackgroundResource(R.drawable.linkg_bg_nav);
        }
        mTitleTxt.setText(title);
        mRightBtn.setText(rightBtn);
        array.recycle();
        mBackImg.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBackListener != null) {
                    mBackListener.onBack();
                } else {
                    if (getContext() instanceof Activity) {
                        ((Activity) getContext()).finish();
                    }
                }
            }
        });
        mRightIcon.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mQuestionListener != null) {
                    mQuestionListener.onClick();
                }
            }
        });

        mRightBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRightBtnListener != null) {
                    mRightBtnListener.onClick();
                }
            }
        });
    }

    public void setRightBtnVisiable(boolean visiable) {
        mRightBtn.setVisibility(visiable ? VISIBLE : INVISIBLE);
    }

    public void setTitle(CharSequence text) {
        mTitleTxt.setText(text);
    }

    public void setRightBtn(String rightBtn) {
        mRightBtn.setText(rightBtn);
    }

    private OnBackListener mBackListener;
    private OnRightImgListener mQuestionListener;
    private OnRightBtnListener mRightBtnListener;

    public void setOnQuestionListener(OnRightImgListener listener) {
        mQuestionListener = listener;
    }

    public void setOnBackListener(OnBackListener listener) {
        mBackListener = listener;
    }

    public void setOnRightBtnListener(OnRightBtnListener onRightBtnListener) {
        mRightBtnListener = onRightBtnListener;
    }

    public interface OnBackListener {
        void onBack();
    }

    public interface OnRightImgListener {
        void onClick();
    }

    public interface OnRightBtnListener {
        void onClick();
    }
}
